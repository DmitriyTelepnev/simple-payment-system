# Simple payment system #

[![Go Report Card](https://goreportcard.com/badge/bitbucket.org/DmitriyTelepnev/simple-payment-system)](https://goreportcard.com/report/bitbucket.org/DmitriyTelepnev/simple-payment-system)

see [task conditions](TASK_CONDITIONS.md)

## Requirements

* linux/unix environment
* stable internet connection
* docker
* docker-compose

## Contains

* PostgreSQL 12.2
* Golang 1.14 backend
* Vue web

## Run it

```text
docker-compose up --build
```

## Run tests

```text
make test
```

## Advantages and disadvantages

see [advantages and disadvantages](ADVANTAGES_DISADVANTAGES.md)

## Api doc

see [api doc](docs/swagger.json)