package main

import (
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/infrastructure/application"
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/infrastructure/config"
	"log"
)

// @title Simple payment system
// @version 1.0
// @description See TASK_CONDITIONS.md
func main() {
	configuration := config.MustConfigure()
	log.Printf("started with configs %#v\n", configuration)

	app := application.NewApplication(configuration)
	app.Run()
}
