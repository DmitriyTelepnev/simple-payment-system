package main

import (
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/adapter/http/dto/response/purse"
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/adapter/http/dto/response/user"
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/adapter/validation/currency"
	"bytes"
	"encoding/json"
	"github.com/gofrs/uuid"
	"log"
	"net/http"
	"time"
)

func main() {
	name1, _ := uuid.NewV4()
	name2, _ := uuid.NewV4()
	account1 := createPurse(name1.String(), currency.USD, "Novosibirsk", "RU")
	log.Printf("created account %s for %s\n", account1, name1.String())
	account2 := createPurse(name2.String(), currency.RUR, "Novosibirsk", "RU")
	log.Printf("created account %s for %s\n", account2, name2.String())

	createCurrency(currency.USD, time.Now().Format("2006-01-02"), 1)
	createCurrency(currency.EUR, time.Now().Format("2006-01-02"), 1.2)
	createCurrency(currency.RUR, time.Now().Format("2006-01-02"), 0.01)

	err := refill(account1, currency.EUR, 10)
	log.Printf("refill error: %s\n", err)

	err = transfer(account1, account2, currency.RUR, 1)
	log.Printf("transfer error: %s\n", err)
	err = transfer(account1, account2, currency.USD, 1)
	log.Printf("transfer error: %s\n", err)
	err = transfer(account1, account2, currency.EUR, 1)
	log.Printf("transfer error: %s\n", err)

	err = transfer(account1, account2, currency.EUR, 100)
	log.Printf("transfer error: %s\n", err)

	for _, row := range getHistory(account1) {
		log.Printf("history for %s: %#v\n", account1, row)
	}

	for _, row := range getHistory(account2) {
		log.Printf("history for %s: %#v\n", account2, row)
	}
}

func createPurse(name string, currency string, city string, country string) string {
	body, _ := json.Marshal(map[string]string{
		"name":     name,
		"country":  country,
		"city":     city,
		"currency": currency,
	})
	response, err := http.Post("http://localhost:8080/user", "application/json", bytes.NewBuffer(body))
	if err != nil {
		panic(err)
	}

	resp := user.Registration{}

	_ = json.NewDecoder(response.Body).Decode(&resp)

	return resp.Account
}

func createCurrency(currency string, date string, cost float32) bool {
	body, _ := json.Marshal(map[string]interface{}{
		"currency": currency,
		"date":     date,
		"cost":     cost,
	})
	response, err := http.Post("http://localhost:8080/currency", "application/json", bytes.NewBuffer(body))
	if err != nil {
		panic(err)
	}

	return response.StatusCode == http.StatusOK
}

func refill(account string, currency string, amount float32) string {
	body, _ := json.Marshal(map[string]interface{}{
		"account":  account,
		"currency": currency,
		"amount":   amount,
	})
	request, err := http.NewRequest(http.MethodPatch, "http://localhost:8080/purse/refill", bytes.NewBuffer(body))
	if err != nil {
		panic(err)
	}
	request.Header.Set("Content-type", "application/json")

	client := http.Client{}

	response, _ := client.Do(request)
	resp := purse.RefillResponse{}

	_ = json.NewDecoder(response.Body).Decode(&resp)

	return resp.Error
}

func transfer(accountFrom string, accountTo string, currency string, amount float32) string {
	body, _ := json.Marshal(map[string]interface{}{
		"accountFrom": accountFrom,
		"accountTo":   accountTo,
		"currency":    currency,
		"amount":      amount,
	})
	request, _ := http.NewRequest(http.MethodPatch, "http://localhost:8080/purse/transfer", bytes.NewBuffer(body))
	request.Header.Set("Content-type", "application/json")

	client := http.Client{}

	response, _ := client.Do(request)

	resp := purse.TransferResponse{}

	_ = json.NewDecoder(response.Body).Decode(&resp)

	return resp.Error
}

func getHistory(account string) []purse.HistoryRow {
	body, _ := json.Marshal(map[string]interface{}{
		"account": account,
	})
	response, _ := http.Post("http://localhost:8080/purse/history", "application/json", bytes.NewBuffer(body))

	resp := purse.History{}

	_ = json.NewDecoder(response.Body).Decode(&resp)

	//log.Printf("%#v\n", resp)

	return resp.Rows
}
