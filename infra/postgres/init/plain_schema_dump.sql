--
-- PostgreSQL database dump
--

-- Dumped from database version 12.2
-- Dumped by pg_dump version 12.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: currency_costs; Type: TABLE; Schema: public; Owner: payment_system
--

CREATE TABLE public.currency_costs (
    currency_code character(3) NOT NULL,
    date timestamp(0) without time zone NOT NULL,
    amount numeric(10,2) NOT NULL
);


ALTER TABLE public.currency_costs OWNER TO payment_system;

--
-- Name: purse_history; Type: TABLE; Schema: public; Owner: payment_system
--

CREATE TABLE public.purse_history (
    account_to character varying(50) NOT NULL,
    operation_ts timestamp(0) without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    account_from character varying(50),
    operation_type smallint NOT NULL,
    amount bigint NOT NULL,
    currency character(3) NOT NULL,
    currency_course numeric(10,2)
);


ALTER TABLE public.purse_history OWNER TO payment_system;

--
-- Name: purses; Type: TABLE; Schema: public; Owner: payment_system
--

CREATE TABLE public.purses (
    uid bigint NOT NULL,
    account character varying(50) NOT NULL,
    user_id bigint NOT NULL,
    currency_code character(4) NOT NULL,
    balance bigint NOT NULL,
    CONSTRAINT purses_balance_check CHECK (((balance)::numeric >= 0.0))
);


ALTER TABLE public.purses OWNER TO payment_system;

--
-- Name: purses_uid_seq; Type: SEQUENCE; Schema: public; Owner: payment_system
--

CREATE SEQUENCE public.purses_uid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.purses_uid_seq OWNER TO payment_system;

--
-- Name: purses_uid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: payment_system
--

ALTER SEQUENCE public.purses_uid_seq OWNED BY public.purses.uid;


--
-- Name: users; Type: TABLE; Schema: public; Owner: payment_system
--

CREATE TABLE public.users (
    uid bigint NOT NULL,
    name character varying(50) NOT NULL,
    country character(3) NOT NULL,
    city character varying(50) NOT NULL
);


ALTER TABLE public.users OWNER TO payment_system;

--
-- Name: users_uid_seq; Type: SEQUENCE; Schema: public; Owner: payment_system
--

CREATE SEQUENCE public.users_uid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_uid_seq OWNER TO payment_system;

--
-- Name: users_uid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: payment_system
--

ALTER SEQUENCE public.users_uid_seq OWNED BY public.users.uid;


--
-- Name: purses uid; Type: DEFAULT; Schema: public; Owner: payment_system
--

ALTER TABLE ONLY public.purses ALTER COLUMN uid SET DEFAULT nextval('public.purses_uid_seq'::regclass);


--
-- Name: users uid; Type: DEFAULT; Schema: public; Owner: payment_system
--

ALTER TABLE ONLY public.users ALTER COLUMN uid SET DEFAULT nextval('public.users_uid_seq'::regclass);


--
-- Name: purses purses_account_key; Type: CONSTRAINT; Schema: public; Owner: payment_system
--

ALTER TABLE ONLY public.purses
    ADD CONSTRAINT purses_account_key UNIQUE (account);


--
-- Name: purses purses_pkey; Type: CONSTRAINT; Schema: public; Owner: payment_system
--

ALTER TABLE ONLY public.purses
    ADD CONSTRAINT purses_pkey PRIMARY KEY (uid);


--
-- Name: users users_name_key; Type: CONSTRAINT; Schema: public; Owner: payment_system
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_name_key UNIQUE (name);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: payment_system
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (uid);


--
-- Name: currency_costs_date_currency_code_idx; Type: INDEX; Schema: public; Owner: payment_system
--

CREATE UNIQUE INDEX currency_costs_date_currency_code_idx ON public.currency_costs USING btree (date, currency_code);


--
-- Name: purse_history_account_to_operation_ts_idx; Type: INDEX; Schema: public; Owner: payment_system
--

CREATE INDEX purse_history_account_to_operation_ts_idx ON public.purse_history USING btree (account_to, operation_ts);


--
-- Name: purses purses_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: payment_system
--

ALTER TABLE ONLY public.purses
    ADD CONSTRAINT purses_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(uid);


--
-- PostgreSQL database dump complete
--

