#!/bin/bash

docker exec -it simple-payment-system_postgres_1 pg_dump -U payment_system -F p -d payment_system -s > ./init/plain_schema_dump.sql