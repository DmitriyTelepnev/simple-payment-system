package mock

import (
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/adapter/validation/currency"
	"fmt"
	"math"
)

type converter struct {
	currencyUSDCosts map[string]float64
}

func NewConverter(currencyUSDCosts map[string]float64) *converter {
	return &converter{
		currencyUSDCosts: currencyUSDCosts,
	}
}

func (c *converter) Convert(amount int64, currencyFrom string, currencyTo string) (int64, error) {
	usdCost, ok := c.currencyUSDCosts[currency.USD]
	if !ok {
		return 0, fmt.Errorf("currency course for %s not found", currencyFrom)
	}

	currencyFromCost, ok := c.currencyUSDCosts[currencyFrom]
	if !ok {
		return 0, fmt.Errorf("currency course for %s not found", currencyFrom)
	}

	currencyToCost, ok := c.currencyUSDCosts[currencyTo]
	if !ok {
		return 0, fmt.Errorf("currency course for %s not found", currencyTo)
	}

	amountInUSD := float64(amount) / usdCost
	amountInCurrencyFrom := math.Round(amountInUSD * currencyFromCost)
	amountInCurrencyTo := math.Round(amountInCurrencyFrom / currencyToCost)

	return int64(amountInCurrencyTo), nil
}
