package currency

import (
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/adapter/validation/currency"
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/domain/repository"
	"fmt"
	"math"
	"time"
)

type converter struct {
	currencyCostRepo repository.CurrencyCost
}

func NewConverter(currencyCostRepo repository.CurrencyCost) *converter {
	return &converter{currencyCostRepo: currencyCostRepo}
}

func (c *converter) Convert(amount int64, currencyFrom string, currencyTo string) (int64, error) {
	today, _ := time.Parse("2006-01-02", time.Now().Format("2006-01-02"))
	usdCost, err := c.currencyCostRepo.Get(currency.USD, today)
	if err != nil || usdCost == nil {
		return 0, fmt.Errorf("base currency not found")
	}

	if usdCost.Cost <= 0 {
		return 0, fmt.Errorf("base currency is invalid")
	}

	currencyFromCost, err := c.currencyCostRepo.Get(currencyFrom, today)
	if err != nil || currencyFromCost == nil {
		return 0, fmt.Errorf("currency cost %s not found", currencyFrom)
	}

	if currencyFromCost.Cost <= 0 {
		return 0, fmt.Errorf("source currency is invalid")
	}

	currencyToCost, err := c.currencyCostRepo.Get(currencyTo, today)
	if err != nil || currencyToCost == nil {
		return 0, fmt.Errorf("currency cost %s not found", currencyTo)
	}

	if currencyToCost.Cost <= 0 {
		return 0, fmt.Errorf("destination currency is invalid")
	}

	amountInUSD := float64(amount) / usdCost.Cost
	amountInCurrencyFrom := amountInUSD * currencyFromCost.Cost
	amountInCurrencyTo := math.Round(amountInCurrencyFrom / currencyToCost.Cost)

	return int64(amountInCurrencyTo), nil
}
