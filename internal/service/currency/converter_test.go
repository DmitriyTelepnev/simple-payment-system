package currency

import (
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/adapter/repository/mock"
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/adapter/validation/currency"
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/domain/entity"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func TestConverter_Convert(t *testing.T) {
	currencyRepo := mock.NewCurrencyCost(nil, nil)

	today, _ := time.Parse("2006-01-02", time.Now().Format("2006-01-02"))

	currencyRepo.Save(entity.CurrencyCost{
		Currency: currency.USD,
		Cost:     1,
		Date:     today,
	})

	currencyRepo.Save(entity.CurrencyCost{
		Currency: currency.EUR,
		Cost:     1.2,
		Date:     today,
	})

	currencyRepo.Save(entity.CurrencyCost{
		Currency: currency.RUR,
		Cost:     0.01,
		Date:     today,
	})

	converter := NewConverter(currencyRepo)

	quotation, _ := converter.Convert(100, currency.RUR, currency.USD)
	assert.Equal(t, int64(1), quotation)

	quotation, _ = converter.Convert(100, currency.EUR, currency.USD)
	assert.Equal(t, int64(120), quotation)

	quotation, _ = converter.Convert(1, currency.EUR, currency.RUR)
	assert.Equal(t, int64(120), quotation)

	quotation, _ = converter.Convert(120, currency.RUR, currency.EUR)
	assert.Equal(t, int64(1), quotation)
}

func TestConverter_Convert_Errors(t *testing.T) {
	currencyRepo := mock.NewCurrencyCost(nil, nil)

	today, _ := time.Parse("2006-01-02", time.Now().Format("2006-01-02"))

	converter := NewConverter(currencyRepo)
	_, err := converter.Convert(1, currency.EUR, currency.RUR)
	assert.Errorf(t, err, "base currency not found")

	currencyRepo.Save(entity.CurrencyCost{
		Currency: currency.USD,
		Cost:     0,
		Date:     today,
	})

	_, err = converter.Convert(1, currency.EUR, currency.RUR)
	assert.Errorf(t, err, "base currency is invalid")

	currencyRepo.Save(entity.CurrencyCost{
		Currency: currency.USD,
		Cost:     1,
		Date:     today,
	})

	_, err = converter.Convert(1, currency.EUR, currency.RUR)
	assert.Errorf(t, err, "currency cost EUR not found")

	currencyRepo.Save(entity.CurrencyCost{
		Currency: currency.EUR,
		Cost:     0,
		Date:     today,
	})

	_, err = converter.Convert(1, currency.EUR, currency.RUR)
	assert.Errorf(t, err, "source currency is invalid")

	currencyRepo.Save(entity.CurrencyCost{
		Currency: currency.EUR,
		Cost:     1.2,
		Date:     today,
	})

	_, err = converter.Convert(1, currency.EUR, currency.RUR)
	assert.Errorf(t, err, "currency cost RUR not found")

	currencyRepo.Save(entity.CurrencyCost{
		Currency: currency.RUR,
		Cost:     0,
		Date:     today,
	})

	_, err = converter.Convert(1, currency.EUR, currency.RUR)
	assert.Errorf(t, err, "destination currency is invalid")

	currencyRepo.Save(entity.CurrencyCost{
		Currency: currency.RUR,
		Cost:     1,
		Date:     today,
	})

	_, err = converter.Convert(1, currency.EUR, currency.RUR)
	assert.NoError(t, err)
}
