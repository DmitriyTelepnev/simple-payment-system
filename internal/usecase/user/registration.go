package user

import (
	userDto "bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/adapter/http/dto/request/user"
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/adapter/usecase"
	dtoValidation "bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/adapter/validation/http/dto"
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/domain/entity"
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/domain/repository"
	"errors"
	"fmt"
	"github.com/gofrs/uuid"
	"log"
	"time"
)

type Registration struct {
	userRepo         repository.User
	purseRepo        repository.Purse
	purseHistoryRepo repository.PurseHistory
	withTransaction  usecase.WithTransactionFunc
}

func NewRegistration(
	userRepo repository.User,
	purseRepo repository.Purse,
	purseHistoryRepo repository.PurseHistory,
	withTransaction usecase.WithTransactionFunc,
) *Registration {
	return &Registration{
		userRepo:         userRepo,
		purseRepo:        purseRepo,
		purseHistoryRepo: purseHistoryRepo,
		withTransaction:  withTransaction,
	}
}

var InvalidRequest = errors.New("registration request is invalid")
var UserAlreadyExists = errors.New("user already exists")
var CantSaveUser = errors.New("can't create user")
var CantGenerateAccount = errors.New("can't generate purse account")
var CantCreatePurse = errors.New("can't create purse")

func (usecase *Registration) Execute(lead userDto.Lead) (string, error) {
	if !dtoValidation.IsLeadValid(lead) {
		return "", InvalidRequest
	}

	user := userDto.LeadToUser(lead)

	var account string

	usecaseError := usecase.withTransaction(func() error {
		oldUser, err := usecase.userRepo.Get(user.Name)
		if err != nil {
			log.Printf("[registration] can't get user: %#v\n", err)
			return fmt.Errorf("something went wrong")
		}

		if oldUser != nil && oldUser.Name == lead.Name {
			return UserAlreadyExists
		}

		err = usecase.userRepo.Save(user)
		if err != nil {
			log.Printf("[registration] can't save user: %#v\n", err)
			return CantSaveUser
		}

		savedUser, _ := usecase.userRepo.Get(user.Name)

		accountUUID, generateAccountErr := uuid.NewV4()
		if generateAccountErr != nil {
			log.Printf("[registration] can't generate account: %#v\n", generateAccountErr)
			return CantGenerateAccount
		}

		purse := entity.Purse{
			Account:  accountUUID.String(),
			UserUID:  savedUser.UID,
			Currency: lead.Currency,
			Balance:  0,
		}
		err = usecase.purseRepo.Save(purse)
		if err != nil {
			log.Printf("[registration] can't create purse: %#v\n", err)
			return CantCreatePurse
		}

		err = usecase.purseHistoryRepo.Save(entity.PurseHistoryRow{
			AccountTo:      purse.Account,
			OperationTime:  time.Now(),
			AccountFrom:    "",
			OperationType:  entity.OperationCreate,
			Amount:         purse.Balance,
			Currency:       purse.Currency,
			CurrencyCourse: 0,
		})
		if err != nil {
			log.Printf("[registration] can't write history: %#v\n", err)
			return CantCreatePurse
		}

		account = purse.Account

		return nil
	})

	return account, usecaseError
}
