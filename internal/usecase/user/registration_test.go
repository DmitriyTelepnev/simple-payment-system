package user

import (
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/adapter/http/dto/request/user"
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/adapter/repository/mock"
	transactionMock "bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/adapter/usecase/mock"
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/adapter/validation/currency"
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/domain/entity"
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func TestRegistration_Execute_InvalidLead(t *testing.T) {
	userRepo := mock.NewUserRepo(nil, nil)
	purseRepo := mock.NewPurseRepo(nil, nil)
	purseHistoryRepo := mock.NewPurseHistoryRepo(nil, nil)
	usecase := NewRegistration(userRepo, purseRepo, purseHistoryRepo, transactionMock.WithTransaction)

	lead := user.Lead{
		Name:     "testUser",
		Country:  "Russia",
		City:     "Novosibirsk",
		Currency: "",
	}

	account, err := usecase.Execute(lead)
	assert.Errorf(t, err, "registration request is invalid")
	assert.Equal(t, "", account)
}

func TestRegistration_Execute_ErrorWhenUserGetting(t *testing.T) {
	userRepo := mock.NewUserRepo(fmt.Errorf("error on get"), nil)
	purseRepo := mock.NewPurseRepo(nil, nil)
	purseHistoryRepo := mock.NewPurseHistoryRepo(nil, nil)
	usecase := NewRegistration(userRepo, purseRepo, purseHistoryRepo, transactionMock.WithTransaction)

	lead := user.Lead{
		Name:     "testUser",
		Country:  "Russia",
		City:     "Novosibirsk",
		Currency: currency.USD,
	}

	account, err := usecase.Execute(lead)
	assert.EqualError(t, err, "something went wrong")
	assert.Equal(t, "", account)
}

func TestRegistration_Execute_ErrorWhenUserSaving(t *testing.T) {
	userRepo := mock.NewUserRepo(nil, fmt.Errorf("error on save"))
	purseRepo := mock.NewPurseRepo(nil, nil)
	purseHistoryRepo := mock.NewPurseHistoryRepo(nil, nil)
	usecase := NewRegistration(userRepo, purseRepo, purseHistoryRepo, transactionMock.WithTransaction)

	lead := user.Lead{
		Name:     "testUser",
		Country:  "Russia",
		City:     "Novosibirsk",
		Currency: currency.USD,
	}

	account, err := usecase.Execute(lead)
	assert.EqualError(t, err, "can't create user")
	assert.Equal(t, "", account)
}

func TestRegistration_Execute_ErrorWhenPurseSaving(t *testing.T) {
	userRepo := mock.NewUserRepo(nil, nil)
	purseRepo := mock.NewPurseRepo(nil, fmt.Errorf("error on save"))
	purseHistoryRepo := mock.NewPurseHistoryRepo(nil, nil)
	usecase := NewRegistration(userRepo, purseRepo, purseHistoryRepo, transactionMock.WithTransaction)

	lead := user.Lead{
		Name:     "testUser",
		Country:  "Russia",
		City:     "Novosibirsk",
		Currency: currency.USD,
	}

	account, err := usecase.Execute(lead)
	assert.EqualError(t, err, "can't create purse")
	assert.Equal(t, "", account)
}

func TestRegistration_Execute(t *testing.T) {
	userRepo := mock.NewUserRepo(nil, nil)
	purseRepo := mock.NewPurseRepo(nil, nil)
	purseHistoryRepo := mock.NewPurseHistoryRepo(nil, nil)
	usecase := NewRegistration(userRepo, purseRepo, purseHistoryRepo, transactionMock.WithTransaction)

	lead := user.Lead{
		Name:     "testUser",
		Country:  "Russia",
		City:     "Novosibirsk",
		Currency: currency.USD,
	}

	account, err := usecase.Execute(lead)
	assert.NoError(t, err)
	assert.NotEmpty(t, account)

	user, err := userRepo.Get(lead.Name)
	assert.NoError(t, err)
	assert.Equal(t, user.Name, lead.Name)

	timestampFrom := time.Now().Add(-10 * time.Second).Unix()
	timestampTo := time.Now().Add(10 * time.Second).Unix()

	purseHistory, _ := purseHistoryRepo.GetByDateAndAccount(account, timestampFrom, timestampTo)
	assert.Len(t, purseHistory, 1)
	assert.Equal(t, account, purseHistory[0].AccountTo)
	assert.Equal(t, entity.OperationCreate, purseHistory[0].OperationType)
	assert.Equal(t, lead.Currency, purseHistory[0].Currency)
}

func TestRegistration_Execute_UserExists(t *testing.T) {
	userRepo := mock.NewUserRepo(nil, nil)
	purseRepo := mock.NewPurseRepo(nil, nil)
	purseHistoryRepo := mock.NewPurseHistoryRepo(nil, nil)
	usecase := NewRegistration(userRepo, purseRepo, purseHistoryRepo, transactionMock.WithTransaction)

	lead := user.Lead{
		Name:     "testUser",
		Country:  "Russia",
		City:     "Novosibirsk",
		Currency: currency.USD,
	}

	account, err := usecase.Execute(lead)
	assert.NoError(t, err)
	assert.NotEmpty(t, account)

	account, err = usecase.Execute(lead)
	assert.EqualError(t, err, UserAlreadyExists.Error())
	assert.Equal(t, "", account)
}
