package purse

import (
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/adapter/http/dto/request/purse"
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/adapter/usecase"
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/domain/entity"
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/domain/repository"
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/domain/service/currency"
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/infrastructure/config"
	"log"
	"time"
)

type Transfer struct {
	purseRepo         repository.Purse
	currencyConverter currency.Converter
	purseHistoryRepo  repository.PurseHistory
	withTransaction   usecase.WithTransactionFunc
}

func NewTransfer(
	purseRepo repository.Purse,
	currencyConverter currency.Converter,
	purseHistoryRepo repository.PurseHistory,
	withTransaction usecase.WithTransactionFunc,
) *Transfer {
	return &Transfer{
		purseRepo:         purseRepo,
		currencyConverter: currencyConverter,
		purseHistoryRepo:  purseHistoryRepo,
		withTransaction:   withTransaction,
	}
}

func (usecase *Transfer) Execute(transferDto purse.Transfer) error {
	intAmount := int64(transferDto.Amount * config.CurrencyBaseDenominator)
	return usecase.withTransaction(func() error {
		purseFrom, err := usecase.purseRepo.Get(transferDto.AccountFrom)
		if err != nil || purseFrom == nil {
			log.Printf("[transfer] can't find purse by account %s: %s\n", transferDto.AccountFrom, err)
			return PurseNotFound
		}

		purseTo, err := usecase.purseRepo.Get(transferDto.AccountTo)
		if err != nil || purseTo == nil {
			log.Printf("[transfer] can't find purse by account %s: %s\n", transferDto.AccountTo, err)
			return PurseNotFound
		}

		convertedAmountDebit, err := usecase.currencyConverter.Convert(intAmount, transferDto.Currency, purseFrom.Currency)
		if err != nil {
			log.Printf("[transfer] can't convert currencies: %s", err)
			return currency.ErrCantConvertCurrencies
		}

		if purseFrom.Balance < convertedAmountDebit {
			return InsufficientFunds
		}

		convertedAmountCredit, err := usecase.currencyConverter.Convert(convertedAmountDebit, purseFrom.Currency, purseTo.Currency)
		if err != nil {
			log.Printf("[transfer] can't convert currencies: %s", err)
			return currency.ErrCantConvertCurrencies
		}

		purseFrom.Balance -= convertedAmountDebit
		purseTo.Balance += convertedAmountCredit

		err = usecase.purseRepo.Save(*purseFrom)
		if err != nil {
			return CantTransferMoney
		}
		err = usecase.purseRepo.Save(*purseTo)
		if err != nil {
			return CantTransferMoney
		}

		err = usecase.purseHistoryRepo.Save(entity.PurseHistoryRow{
			AccountTo:      purseFrom.Account,
			OperationTime:  time.Now(),
			AccountFrom:    purseTo.Account,
			OperationType:  entity.OperationTransfer,
			Amount:         -convertedAmountDebit,
			Currency:       purseFrom.Currency,
			CurrencyCourse: 0,
		})
		if err != nil {
			log.Printf("[transfer] can't write history: %#v\n", err)
			return CantTransferMoney
		}

		err = usecase.purseHistoryRepo.Save(entity.PurseHistoryRow{
			AccountTo:      purseTo.Account,
			OperationTime:  time.Now(),
			AccountFrom:    purseFrom.Account,
			OperationType:  entity.OperationTransfer,
			Amount:         convertedAmountCredit,
			Currency:       purseTo.Currency,
			CurrencyCourse: 0,
		})
		if err != nil {
			log.Printf("[transfer] can't write history: %#v\n", err)
			return CantTransferMoney
		}

		return nil
	})
}
