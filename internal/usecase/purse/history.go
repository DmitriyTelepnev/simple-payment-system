package purse

import (
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/domain/entity"
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/domain/repository"
	"errors"
)

type History struct {
	historyRepo repository.PurseHistory
}

func NewHistory(historyRepo repository.PurseHistory) *History {
	return &History{historyRepo: historyRepo}
}

var DateValidationError error = errors.New("dates is invalid")

func (usecase *History) Execute(account string, timestampFrom int64, timestampTo int64) ([]entity.PurseHistoryRow, error) {
	if timestampTo != 0 && timestampFrom > timestampTo {
		return nil, DateValidationError
	}

	return usecase.historyRepo.GetByDateAndAccount(account, timestampFrom, timestampTo)
}
