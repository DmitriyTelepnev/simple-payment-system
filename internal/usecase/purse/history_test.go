package purse

import (
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/adapter/repository/mock"
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/domain/entity"
	"errors"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func TestHistory_Execute_InvalidDates(t *testing.T) {
	historyRepo := mock.NewPurseHistoryRepo(nil, nil)
	usecase := NewHistory(historyRepo)

	tsBefore := time.Now().Add(10 * time.Second).Unix()
	tsAfter := time.Now().Unix()

	rows, err := usecase.Execute("test-account", tsBefore, tsAfter)
	assert.Nil(t, rows)
	assert.Errorf(t, err, DateValidationError.Error())
}

func TestHistory_Execute_ErrorOnGet(t *testing.T) {
	historyRepo := mock.NewPurseHistoryRepo(errors.New("test"), nil)
	usecase := NewHistory(historyRepo)

	tsBefore := time.Now().Unix()
	tsAfter := time.Now().Add(10 * time.Second).Unix()

	rows, err := usecase.Execute("test-account", tsBefore, tsAfter)
	assert.Nil(t, rows)
	assert.Errorf(t, err, "test")
}

func TestHistory_Execute(t *testing.T) {
	historyRepo := mock.NewPurseHistoryRepo(nil, nil)

	account1 := "test-account1"
	account2 := "test-account2"

	historyForAccount1 := []entity.PurseHistoryRow{
		{
			AccountTo:      account1,
			OperationTime:  time.Now(),
			AccountFrom:    "",
			OperationType:  entity.OperationCreate,
			Amount:         0,
			Currency:       "RUR",
			CurrencyCourse: 0,
		},
		{
			AccountTo:      account1,
			OperationTime:  time.Now(),
			AccountFrom:    "",
			OperationType:  entity.OperationRefill,
			Amount:         100,
			Currency:       "RUR",
			CurrencyCourse: 0,
		},
		{
			AccountTo:      account1,
			OperationTime:  time.Now(),
			AccountFrom:    account2,
			OperationType:  entity.OperationTransfer,
			Amount:         -50,
			Currency:       "RUR",
			CurrencyCourse: 0,
		},
	}

	historyForAccount2 := []entity.PurseHistoryRow{
		{
			AccountTo:      account2,
			OperationTime:  time.Now(),
			AccountFrom:    "",
			OperationType:  entity.OperationCreate,
			Amount:         0,
			Currency:       "RUR",
			CurrencyCourse: 0,
		},
		{
			AccountTo:      account2,
			OperationTime:  time.Now(),
			AccountFrom:    account1,
			OperationType:  entity.OperationTransfer,
			Amount:         50,
			Currency:       "RUR",
			CurrencyCourse: 0,
		},
	}

	for _, row := range historyForAccount1 {
		_ = historyRepo.Save(row)
	}

	for _, row := range historyForAccount2 {
		_ = historyRepo.Save(row)
	}

	usecase := NewHistory(historyRepo)

	tsBefore := time.Now().Add(-10 * time.Second).Unix()
	tsAfter := time.Now().Add(10 * time.Second).Unix()

	rows, err := usecase.Execute(account1, tsBefore, tsAfter)
	assert.NoError(t, err)
	assert.Equal(t, historyForAccount1, rows)

	rows, err = usecase.Execute(account2, tsBefore, tsAfter)
	assert.NoError(t, err)
	assert.Equal(t, historyForAccount2, rows)
}
