package purse

import "errors"

var PurseNotFound error = errors.New("purse not found")
var CantRefillPurse error = errors.New("can't refill purse")
var InsufficientFunds error = errors.New("insufficient funds")
var CantTransferMoney error = errors.New("cant't transfer money")
