package purse

import (
	dto "bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/adapter/http/dto/request/purse"
	repoMock "bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/adapter/repository/mock"
	transactionMock "bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/adapter/usecase/mock"
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/adapter/validation/currency"
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/domain/entity"
	converter "bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/domain/service/currency"
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/infrastructure/config"
	converterMock "bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/service/currency/mock"
	"errors"
	"github.com/stretchr/testify/assert"
	"math"
	"testing"
	"time"
)

func TestRefill_Execute_EmptyPurse(t *testing.T) {
	purseRepo := repoMock.NewPurseRepo(nil, nil)
	purse := entity.Purse{
		Account:  "test-purse-account",
		UserUID:  1,
		Currency: currency.EUR,
		Balance:  0,
	}
	_ = purseRepo.Save(purse)

	currencyCosts := map[string]float64{
		currency.EUR: 1.2,
		currency.USD: 1,
		currency.RUR: 0.01,
	}
	currencyConverter := converterMock.NewConverter(currencyCosts)

	purseHistoryRepo := repoMock.NewPurseHistoryRepo(nil, nil)

	currencyExpected := currency.USD

	amount := float64(10)

	usecase := NewRefill(purseRepo, currencyConverter, purseHistoryRepo, transactionMock.WithTransaction)
	refillDto := dto.Refill{
		Account:  purse.Account,
		Currency: currencyExpected,
		Amount:   amount,
	}
	_ = usecase.Execute(refillDto)

	updatedPurse, _ := purseRepo.Get(purse.Account)

	currencyCostFrom, _ := currencyCosts[currencyExpected]
	currencyCostTo, _ := currencyCosts[purse.Currency]
	res := purse.Balance + (int64(amount * config.CurrencyBaseDenominator * currencyCostFrom / currencyCostTo))
	assert.Equal(t, res, updatedPurse.Balance)

	timestampFrom := time.Now().Add(-10 * time.Second).Unix()
	timestampTo := time.Now().Add(10 * time.Second).Unix()

	purseHistory, _ := purseHistoryRepo.GetByDateAndAccount(purse.Account, timestampFrom, timestampTo)
	assert.Len(t, purseHistory, 1)
	assert.Equal(t, purse.Account, purseHistory[0].AccountTo)
	assert.Equal(t, entity.OperationRefill, purseHistory[0].OperationType)
	assert.Equal(t, currencyExpected, purseHistory[0].Currency)
}

func TestRefill_Execute_AddToBalance(t *testing.T) {
	purseRepo := repoMock.NewPurseRepo(nil, nil)
	purse := entity.Purse{
		Account:  "test-purse-account",
		UserUID:  1,
		Currency: currency.EUR,
		Balance:  10,
	}
	_ = purseRepo.Save(purse)

	currencyCosts := map[string]float64{
		currency.EUR: 1.2,
		currency.USD: 1,
		currency.RUR: 0.2,
	}
	currencyConverter := converterMock.NewConverter(currencyCosts)

	purseHistoryRepo := repoMock.NewPurseHistoryRepo(nil, nil)

	amount := float64(10)

	usecase := NewRefill(purseRepo, currencyConverter, purseHistoryRepo, transactionMock.WithTransaction)
	refillDto := dto.Refill{
		Account:  purse.Account,
		Currency: currency.USD,
		Amount:   amount,
	}
	_ = usecase.Execute(refillDto)

	updatedPurse, _ := purseRepo.Get(purse.Account)

	currencyCostFrom, _ := currencyCosts[currency.USD]
	currencyCostTo, _ := currencyCosts[purse.Currency]
	res := purse.Balance + int64(math.Round(amount*config.CurrencyBaseDenominator*currencyCostFrom/currencyCostTo))
	assert.Equal(t, res, updatedPurse.Balance)
}

func TestRefill_Execute_UnknownAccount(t *testing.T) {
	purseRepo := repoMock.NewPurseRepo(nil, nil)
	purse := entity.Purse{
		Account:  "test-purse-account",
		UserUID:  1,
		Currency: currency.EUR,
		Balance:  10,
	}
	_ = purseRepo.Save(purse)

	currencyCosts := map[string]float64{
		currency.EUR: 1.2,
		currency.USD: 1,
		currency.RUR: 0.01,
	}
	currencyConverter := converterMock.NewConverter(currencyCosts)

	purseHistoryRepo := repoMock.NewPurseHistoryRepo(nil, nil)

	usecase := NewRefill(purseRepo, currencyConverter, purseHistoryRepo, transactionMock.WithTransaction)
	refillDto := dto.Refill{
		Account:  "unknown-account",
		Currency: currency.USD,
		Amount:   10,
	}
	err := usecase.Execute(refillDto)
	assert.EqualError(t, err, PurseNotFound.Error())
}

func TestRefill_Execute_UnknownCurrency(t *testing.T) {
	purseRepo := repoMock.NewPurseRepo(nil, nil)
	purse := entity.Purse{
		Account:  "test-purse-account",
		UserUID:  1,
		Currency: currency.EUR,
		Balance:  10,
	}
	_ = purseRepo.Save(purse)

	currencyCosts := map[string]float64{
		currency.EUR: 1.2,
		currency.USD: 1,
		currency.RUR: 0.01,
	}
	currencyConverter := converterMock.NewConverter(currencyCosts)

	purseHistoryRepo := repoMock.NewPurseHistoryRepo(nil, nil)

	usecase := NewRefill(purseRepo, currencyConverter, purseHistoryRepo, transactionMock.WithTransaction)
	refillDto := dto.Refill{
		Account:  purse.Account,
		Currency: "YEN",
		Amount:   10,
	}
	err := usecase.Execute(refillDto)
	assert.EqualError(t, err, currency.CurrencyNotSupported.Error())
}

func TestRefill_Execute_CurrencyCourseNotFound(t *testing.T) {
	purseRepo := repoMock.NewPurseRepo(nil, nil)
	purse := entity.Purse{
		Account:  "test-purse-account",
		UserUID:  1,
		Currency: currency.EUR,
		Balance:  10,
	}
	_ = purseRepo.Save(purse)

	currencyCosts := map[string]float64{
		currency.USD: 1,
		currency.RUR: 0.01,
	}
	currencyConverter := converterMock.NewConverter(currencyCosts)

	purseHistoryRepo := repoMock.NewPurseHistoryRepo(nil, nil)

	usecase := NewRefill(purseRepo, currencyConverter, purseHistoryRepo, transactionMock.WithTransaction)
	refillDto := dto.Refill{
		Account:  purse.Account,
		Currency: currency.EUR,
		Amount:   10,
	}
	err := usecase.Execute(refillDto)
	assert.EqualError(t, err, converter.ErrCantConvertCurrencies.Error())
}

func TestRefill_Execute_ErrorWhenGetPurse(t *testing.T) {
	purseRepo := repoMock.NewPurseRepo(errors.New("test"), nil)
	purse := entity.Purse{
		Account:  "test-purse-account",
		UserUID:  1,
		Currency: currency.EUR,
		Balance:  10,
	}
	_ = purseRepo.Save(purse)

	currencyCosts := map[string]float64{
		currency.USD: 1,
		currency.RUR: 0.01,
	}
	currencyConverter := converterMock.NewConverter(currencyCosts)

	purseHistoryRepo := repoMock.NewPurseHistoryRepo(nil, nil)

	usecase := NewRefill(purseRepo, currencyConverter, purseHistoryRepo, transactionMock.WithTransaction)
	refillDto := dto.Refill{
		Account:  purse.Account,
		Currency: currency.EUR,
		Amount:   10,
	}
	err := usecase.Execute(refillDto)
	assert.EqualError(t, err, PurseNotFound.Error())
}

func TestRefill_Execute_CantWriteHistory(t *testing.T) {
	purseRepo := repoMock.NewPurseRepo(nil, nil)
	purse := entity.Purse{
		Account:  "test-purse-account",
		UserUID:  1,
		Currency: currency.EUR,
		Balance:  10,
	}
	_ = purseRepo.Save(purse)

	currencyCosts := map[string]float64{
		currency.USD: 1,
		currency.RUR: 0.01,
	}
	currencyConverter := converterMock.NewConverter(currencyCosts)

	purseHistoryRepo := repoMock.NewPurseHistoryRepo(errors.New("test"), nil)

	usecase := NewRefill(purseRepo, currencyConverter, purseHistoryRepo, transactionMock.WithTransaction)
	refillDto := dto.Refill{
		Account:  purse.Account,
		Currency: currency.EUR,
		Amount:   10,
	}
	err := usecase.Execute(refillDto)
	assert.EqualError(t, err, converter.ErrCantConvertCurrencies.Error())
}
