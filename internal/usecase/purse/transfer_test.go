package purse

import (
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/adapter/http/dto/request/purse"
	repoMock "bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/adapter/repository/mock"
	transactionMock "bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/adapter/usecase/mock"
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/adapter/validation/currency"
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/domain/entity"
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/infrastructure/config"
	converterMock "bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/service/currency/mock"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func TestTransfer_Execute(t *testing.T) {
	purseRepo := repoMock.NewPurseRepo(nil, nil)
	purseFrom := entity.Purse{
		Account:  "test-purse-account-from",
		UserUID:  1,
		Currency: currency.EUR,
		Balance:  10 * config.CurrencyBaseDenominator,
	}
	_ = purseRepo.Save(purseFrom)

	purseTo := entity.Purse{
		Account:  "test-purse-account-to",
		UserUID:  1,
		Currency: currency.EUR,
		Balance:  10 * config.CurrencyBaseDenominator,
	}
	_ = purseRepo.Save(purseTo)

	currencyCosts := map[string]float64{
		currency.EUR: 1.2,
		currency.USD: 1,
		currency.RUR: 0.01,
	}
	currencyConverter := converterMock.NewConverter(currencyCosts)

	purseHistoryRepo := repoMock.NewPurseHistoryRepo(nil, nil)

	usecase := NewTransfer(purseRepo, currencyConverter, purseHistoryRepo, transactionMock.WithTransaction)
	transferDto := purse.Transfer{
		AccountFrom: purseFrom.Account,
		AccountTo:   purseTo.Account,
		Amount:      5,
		Currency:    currency.EUR,
	}
	usecaseError := usecase.Execute(transferDto)
	assert.NoError(t, usecaseError)

	updatedPurseFrom, _ := purseRepo.Get(purseFrom.Account)
	updatedPurseTo, _ := purseRepo.Get(purseTo.Account)

	assert.Equal(t, int64(500), updatedPurseFrom.Balance)
	assert.Equal(t, int64(1500), updatedPurseTo.Balance)
}

func TestTransfer_Execute_WithDifferentCurrency(t *testing.T) {
	purseRepo := repoMock.NewPurseRepo(nil, nil)
	purseFrom := entity.Purse{
		Account:  "test-purse-account-from",
		UserUID:  1,
		Currency: currency.EUR,
		Balance:  10 * config.CurrencyBaseDenominator,
	}
	_ = purseRepo.Save(purseFrom)

	purseTo := entity.Purse{
		Account:  "test-purse-account-to",
		UserUID:  1,
		Currency: currency.USD,
		Balance:  10 * config.CurrencyBaseDenominator,
	}
	_ = purseRepo.Save(purseTo)

	currencyCosts := map[string]float64{
		currency.EUR: 1.2,
		currency.USD: 1,
		currency.RUR: 0.01,
	}
	currencyConverter := converterMock.NewConverter(currencyCosts)

	purseHistoryRepo := repoMock.NewPurseHistoryRepo(nil, nil)

	usecase := NewTransfer(purseRepo, currencyConverter, purseHistoryRepo, transactionMock.WithTransaction)
	transferDto := purse.Transfer{
		AccountFrom: purseFrom.Account,
		AccountTo:   purseTo.Account,
		Amount:      5,
		Currency:    currency.EUR,
	}
	usecaseError := usecase.Execute(transferDto)
	assert.NoError(t, usecaseError)

	updatedPurseFrom, _ := purseRepo.Get(purseFrom.Account)
	updatedPurseTo, _ := purseRepo.Get(purseTo.Account)

	assert.Equal(t, int64(500), updatedPurseFrom.Balance)
	assert.Equal(t, int64(1600), updatedPurseTo.Balance)

	timestampFrom := time.Now().Add(-10 * time.Second).Unix()
	timestampTo := time.Now().Add(10 * time.Second).Unix()

	purseHistoryFrom, _ := purseHistoryRepo.GetByDateAndAccount(purseFrom.Account, timestampFrom, timestampTo)
	assert.Len(t, purseHistoryFrom, 1)
	assert.Equal(t, purseFrom.Account, purseHistoryFrom[0].AccountTo)
	assert.Equal(t, entity.OperationTransfer, purseHistoryFrom[0].OperationType)
	assert.Equal(t, purseFrom.Currency, purseHistoryFrom[0].Currency)

	purseHistoryTo, _ := purseHistoryRepo.GetByDateAndAccount(purseTo.Account, timestampFrom, timestampTo)
	assert.Len(t, purseHistoryTo, 1)
	assert.Equal(t, purseTo.Account, purseHistoryTo[0].AccountTo)
	assert.Equal(t, entity.OperationTransfer, purseHistoryTo[0].OperationType)
	assert.Equal(t, purseTo.Currency, purseHistoryTo[0].Currency)
}
