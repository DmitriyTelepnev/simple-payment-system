package purse

import (
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/adapter/http/dto/request/purse"
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/adapter/usecase"
	validation "bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/adapter/validation/currency"
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/domain/entity"
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/domain/repository"
	currencyServices "bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/domain/service/currency"
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/infrastructure/config"
	"log"
	"time"
)

type Refill struct {
	purseRepo         repository.Purse
	currencyConverter currencyServices.Converter
	purseHistoryRepo  repository.PurseHistory
	withTransaction   usecase.WithTransactionFunc
}

func NewRefill(
	purseRepo repository.Purse,
	currencyConverter currencyServices.Converter,
	purseHistoryRepo repository.PurseHistory,
	withTransaction usecase.WithTransactionFunc,
) *Refill {
	return &Refill{
		purseRepo:         purseRepo,
		currencyConverter: currencyConverter,
		purseHistoryRepo:  purseHistoryRepo,
		withTransaction:   withTransaction,
	}
}

func (usecase *Refill) Execute(refillDto purse.Refill) error {
	if !validation.IsValidCurrency(refillDto.Currency) {
		return validation.CurrencyNotSupported
	}

	intAmount := int64(refillDto.Amount * config.CurrencyBaseDenominator)

	return usecase.withTransaction(func() error {
		purse, err := usecase.purseRepo.Get(refillDto.Account)
		if err != nil || purse == nil {
			log.Printf("[refill] can't find purse by %s: %s\n", refillDto.Account, err)
			return PurseNotFound
		}

		additionalAmount, err := usecase.currencyConverter.Convert(intAmount, refillDto.Currency, purse.Currency)
		if err != nil {
			log.Printf("[refill] can't convert currencies: %s\n", err)
			return currencyServices.ErrCantConvertCurrencies
		}

		purse.Balance += additionalAmount

		err = usecase.purseRepo.Save(*purse)
		if err != nil {
			log.Printf("[refill] can't save purse: %s\n", err)
			return CantRefillPurse
		}

		err = usecase.purseHistoryRepo.Save(entity.PurseHistoryRow{
			AccountTo:      purse.Account,
			OperationTime:  time.Now(),
			AccountFrom:    "",
			OperationType:  entity.OperationRefill,
			Amount:         intAmount,
			Currency:       refillDto.Currency,
			CurrencyCourse: 0,
		})
		if err != nil {
			log.Printf("[refill] can't write history: %#v\n", err)
			return CantRefillPurse
		}

		return nil
	})
}
