package currency

import (
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/adapter/repository/mock"
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/adapter/validation/currency"
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func TestUpload_Execute_InvalidCurrency(t *testing.T) {
	currencyCostRepo := mock.NewCurrencyCost(nil, nil)
	usecase := NewUpload(currencyCostRepo)

	date, _ := time.Parse("2006-01-02", "2020-01-01")

	err := usecase.Execute("YEN", 0.3, date)
	assert.EqualError(t, err, currency.CurrencyNotSupported.Error())
}

func TestUpload_Execute_CurrencyWithZeroCost(t *testing.T) {
	currencyCostRepo := mock.NewCurrencyCost(nil, fmt.Errorf("something wrong"))
	usecase := NewUpload(currencyCostRepo)

	date, _ := time.Parse("2006-01-02", "2020-01-01")

	err := usecase.Execute(currency.USD, 0, date)
	assert.EqualError(t, err, CostIsLessThanOrEqualZero.Error())
}

func TestUpload_Execute_ErrorWhenSavingCurrency(t *testing.T) {
	currencyCostRepo := mock.NewCurrencyCost(nil, fmt.Errorf("something wrong"))
	usecase := NewUpload(currencyCostRepo)

	date, _ := time.Parse("2006-01-02", "2020-01-01")

	err := usecase.Execute(currency.USD, 0.3, date)
	assert.EqualError(t, err, "can't save currency cost")
}

func TestUpload_Execute(t *testing.T) {
	currencyCostRepo := mock.NewCurrencyCost(nil, nil)
	usecase := NewUpload(currencyCostRepo)

	date, _ := time.Parse("2006-01-02", "2020-01-01")

	costValue := float64(0.3)

	err := usecase.Execute(currency.USD, costValue, date)
	assert.NoError(t, err)

	cost, _ := currencyCostRepo.Get(currency.USD, date)
	assert.Equal(t, costValue, cost.Cost)
}
