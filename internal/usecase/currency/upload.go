package currency

import (
	validation "bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/adapter/validation/currency"
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/domain/entity"
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/domain/repository"
	"errors"
	"log"
	"time"
)

type Upload struct {
	currencyCostRepo repository.CurrencyCost
}

var CostIsLessThanOrEqualZero = errors.New("cost is less than or equal to zero")

func NewUpload(currencyCostRepo repository.CurrencyCost) *Upload {
	return &Upload{
		currencyCostRepo: currencyCostRepo,
	}
}

func (usecase *Upload) Execute(currency string, cost float64, date time.Time) error {
	if !validation.IsValidCurrency(currency) {
		return validation.CurrencyNotSupported
	}

	if cost <= 0 {
		return CostIsLessThanOrEqualZero
	}

	currencyCost := entity.CurrencyCost{
		Currency: currency,
		Cost:     cost,
		Date:     date,
	}

	err := usecase.currencyCostRepo.Save(currencyCost)
	if err != nil {
		log.Printf("[currency upload] can't save currency: %s", err)
		return repository.ErrCantSaveCurrency
	}

	return nil
}
