package signal

import (
	"os"
	"os/signal"
	"syscall"
)

type signalHandler struct {
	signalChan chan os.Signal
	callback   func() error
}

func NewSignalHandler(callback func() error) *signalHandler {
	signalChan := make(chan os.Signal)
	signal.Notify(signalChan, syscall.SIGINT, syscall.SIGTERM)

	return &signalHandler{
		signalChan: signalChan,
		callback:   callback,
	}
}

func (handler *signalHandler) Poll() {
	<-handler.signalChan
	callbackError := handler.callback()
	if callbackError != nil {
		panic(callbackError)
	}
}
