package database

import (
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/infrastructure/config"
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/infrastructure/retryer"
	"context"
	"fmt"
	"github.com/jackc/pgx/v4"
	"github.com/pkg/errors"
	"log"
	"time"
)

var ErrCantInstantiateConnection = errors.New("can't instantiate connection")
var ErrPing = errors.New("can't ping dbms")

const (
	connectTimeout = 10 * time.Second
	pingTimeout    = 3 * time.Second
)

func MustPostgresConnection(config *config.PostgreSQL) *pgx.Conn {
	dsn := fmt.Sprintf(
		"postgres://%s:%s@%s:%d/%s?sslmode=disable",
		config.UserName,
		config.Password,
		config.Host,
		config.Port,
		config.DatabaseName,
	)

	var db *pgx.Conn
	err := retryer.WithRetry(func() error {
		ctx, cancelConnect := context.WithTimeout(context.Background(), connectTimeout)
		defer cancelConnect()
		var instantiateErr error
		db, instantiateErr = pgx.Connect(ctx, dsn)
		if instantiateErr != nil {
			log.Printf("[connect to postgres] %s", instantiateErr)
			return ErrCantInstantiateConnection
		}

		pingCtx, pingCancel := context.WithTimeout(context.Background(), pingTimeout)
		defer pingCancel()
		pingError := db.Ping(pingCtx)
		if pingError != nil {
			return pingError
		}

		return nil
	}, 10, ErrCantInstantiateConnection, ErrPing)

	if err != nil {
		panic(err)
	}

	return db
}
