package config

import (
	"github.com/pkg/errors"
	"gopkg.in/yaml.v3"
	"io/ioutil"
	"time"
)

const defaultConfigPath = "./config.yaml"

const CurrencyBaseDenominator = 100

type (
	PostgreSQL struct {
		Host         string `yaml:"host"`
		UserName     string `yaml:"username"`
		Password     string `yaml:"password"`
		DatabaseName string `yaml:"databasename"`
		Port         int    `yaml:"port"`
	}

	Currencies struct {
		Capacity int `yaml:"capacity"`
		TTL      int `yaml:"ttl"`
	}

	Cache struct {
		Currencies Currencies `yaml:"currencies"`
	}

	Application struct {
		PostrgreSQL PostgreSQL `yaml:"postgresql"`
		Cache       Cache      `yaml:"cache"`
	}
)

func MustConfigure() *Application {
	cfg := Application{}
	reader, err := ioutil.ReadFile(defaultConfigPath)
	if err != nil {
		panic(errors.Wrap(err, "can't read configs"))
	}
	err = yaml.Unmarshal(reader, &cfg)
	if err != nil {
		panic(errors.Wrap(err, "can't unmarshal config"))
	}
	return &cfg
}

func (c *Currencies) GetCapacity() int {
	return c.Capacity
}

func (c *Currencies) GetTTL() time.Duration {
	return time.Duration(c.TTL) * time.Second
}
