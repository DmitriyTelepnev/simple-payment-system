package cache

import "time"

type cachedObject struct {
	obj       interface{}
	createdAt time.Time
}

type Cache interface {
	Set(key string, obj interface{}) error
	Get(key string) (interface{}, error)
}
