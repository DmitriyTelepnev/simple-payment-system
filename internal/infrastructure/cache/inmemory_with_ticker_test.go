package cache

import (
	"github.com/stretchr/testify/assert"
	"math/rand"
	"testing"
	"time"
)

func TestInmemoryWithTicker(t *testing.T) {
	inmemory := NewInmemoryWithTicker(3, 1*time.Hour)
	inmemory.Set("a", 1)
	inmemory.Set("b", 2)
	inmemory.Set("c", 3)
	inmemory.Set("d", 4)
	inmemory.Set("e", 5)

	obj, err := inmemory.Get("a")
	assert.Nil(t, obj)
	assert.Errorf(t, err, "object not exists")

	obj, err = inmemory.Get("b")
	assert.Nil(t, obj)
	assert.Errorf(t, err, "object not exists")

	obj, err = inmemory.Get("c")
	assert.NoError(t, err)
	assert.Equal(t, 3, obj.(int))

	obj, err = inmemory.Get("d")
	assert.NoError(t, err)
	assert.Equal(t, 4, obj.(int))

	obj, err = inmemory.Get("e")
	assert.NoError(t, err)
	assert.Equal(t, 5, obj.(int))

	obj, err = inmemory.Get("f")
	assert.Nil(t, obj)
	assert.Errorf(t, err, "object not exists")

	inmemory.Stop()
}

type testObj struct {
	i int64
}

func BenchmarkInmemoryWithTicker_Get(b *testing.B) {
	inmemory := NewInmemoryWithTicker(1000, 3*time.Second)

	for i:=0; i < 1000; i++ {
		inmemory.Set(string(i), testObj{i: int64(i)})
	}

	b.ResetTimer()
	b.ReportAllocs()
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			v := rand.Int63() % 1000
			inmemory.Get(string(v))
		}
	})

	inmemory.Stop()
}

func BenchmarkInmemoryWithTicker_Set(b *testing.B) {
	inmemory := NewInmemoryWithTicker(1000, 1*time.Second)

	b.ResetTimer()
	b.ReportAllocs()
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			v := rand.Int63() % 3000
			inmemory.Set(string(v), testObj{i: v})
		}
	})

	inmemory.Stop()
}

func BenchmarkInmemoryWithTicker(b *testing.B) {
	inmemory := NewInmemoryWithTicker(1000, 1*time.Second)
	b.ReportAllocs()
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			v := rand.Int63() % 2000
			mode := rand.Int()
			if mode%2 == 0 {
				inmemory.Set(string(v), v)
			} else {
				inmemory.Get(string(v))
			}
		}
	})

	inmemory.Stop()
}
