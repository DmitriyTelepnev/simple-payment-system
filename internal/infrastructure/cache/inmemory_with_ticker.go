package cache

import (
	"errors"
	"sync"
	"time"
)

type inmemoryWithTicker struct {
	sync.RWMutex

	capacity int
	ttl      time.Duration
	items    map[string]cachedObject
	ticker   *time.Ticker
}

func NewInmemoryWithTicker(capacity int, ttl time.Duration) *inmemoryWithTicker {
	ticker := time.NewTicker(ttl)

	inmemoryCache := &inmemoryWithTicker{
		capacity: capacity,
		items:    make(map[string]cachedObject, capacity),
		ticker:   ticker,
	}

	go func() {
		for range ticker.C {
			inmemoryCache.cleanUp()
		}
	}()

	return inmemoryCache
}

func (inmemoryCache *inmemoryWithTicker) Stop() {
	inmemoryCache.ticker.Stop()
}

func (inmemoryCache *inmemoryWithTicker) cleanUp() {
	inmemoryCache.RLock()
	defer inmemoryCache.RUnlock()

	for key, obj := range inmemoryCache.items {
		if time.Since(obj.createdAt) > inmemoryCache.ttl {
			delete(inmemoryCache.items, key)
		}
	}
}

func (inmemoryCache *inmemoryWithTicker) Set(key string, obj interface{}) error {
	inmemoryCache.Lock()
	defer inmemoryCache.Unlock()
	_, ok := inmemoryCache.items[key]
	if !ok {
		if len(inmemoryCache.items) >= inmemoryCache.capacity {
			delete(inmemoryCache.items, inmemoryCache.getOldestKey())
		}

		inmemoryCache.items[key] = cachedObject{
			obj:       obj,
			createdAt: time.Now(),
		}
	}

	return nil
}

func (inmemoryCache *inmemoryWithTicker) getOldestKey() string {
	earliestTime := time.Now()
	oldestKey := ""
	for key, obj := range inmemoryCache.items {
		if obj.createdAt.Before(earliestTime) {
			earliestTime = obj.createdAt
			oldestKey = key
		}
	}

	return oldestKey
}

func (inmemoryCache *inmemoryWithTicker) Get(key string) (interface{}, error) {
	inmemoryCache.RLock()
	defer inmemoryCache.RUnlock()

	obj, ok := inmemoryCache.items[key]
	if !ok {
		return nil, errors.New("object not exists")
	}

	return obj.obj, nil
}
