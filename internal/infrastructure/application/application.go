package application

import (
	currencyHandler "bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/adapter/http/handler/currency"
	purseHandler "bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/adapter/http/handler/purse"
	userHandler "bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/adapter/http/handler/user"
	cachedRepo "bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/adapter/repository/cache"
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/adapter/repository/postgres"
	postgresTransaction "bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/adapter/usecase/postgres"
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/infrastructure/cache"
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/infrastructure/config"
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/infrastructure/database"
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/infrastructure/signal"
	converter "bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/service/currency"
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/usecase/currency"
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/usecase/purse"
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/usecase/user"
	"context"
	"fmt"
	"github.com/valyala/fasthttp"
	"log"
	"net/http"
	"sync"
	"time"
)

type application struct {
	configs *config.Application
}

func NewApplication(configs *config.Application) *application {
	return &application{
		configs: configs,
	}
}

func (application *application) Run() {
	postgresConnection := database.MustPostgresConnection(&application.configs.PostrgreSQL)
	fmt.Printf("connected to postgres %v", postgresConnection)

	userRepo := postgres.NewUserRepo(postgresConnection)
	purseRepo := postgres.NewPurseRepo(postgresConnection)
	purseHistoryRepo := postgres.NewPurseHistoryRepo(postgresConnection)

	currencyCostRepo := postgres.NewCurrencyCostRepo(postgresConnection)
	currencyCache := cache.NewInmemoryWithTicker(
		application.configs.Cache.Currencies.GetCapacity(),
		application.configs.Cache.Currencies.GetTTL(),
	)
	cachedCurrencyCostRepo := cachedRepo.NewCachedCurrencyRepo(currencyCostRepo, currencyCache)

	currencyConverter := converter.NewConverter(cachedCurrencyCostRepo)
	withTransaction := postgresTransaction.WithTransaction(postgresConnection)

	userRegistrationUsecase := user.NewRegistration(userRepo, purseRepo, purseHistoryRepo, withTransaction)
	purseRefillUsecase := purse.NewRefill(purseRepo, currencyConverter, purseHistoryRepo, withTransaction)
	purseTransferUsecase := purse.NewTransfer(purseRepo, currencyConverter, purseHistoryRepo, withTransaction)
	purseHistoryUsecase := purse.NewHistory(purseHistoryRepo)
	currencyCostUploadUsecase := currency.NewUpload(cachedCurrencyCostRepo)

	userRegistrationHandler := userHandler.NewRegistration(userRegistrationUsecase)
	purseRefillHandler := purseHandler.NewRefill(purseRefillUsecase)
	purseTransferHandler := purseHandler.NewTransfer(purseTransferUsecase)
	purseHistoryHandler := purseHandler.NewHistory(purseHistoryUsecase)
	currencyUploadHandler := currencyHandler.NewUpload(currencyCostUploadUsecase)

	handlers := func(ctx *fasthttp.RequestCtx) {
		ctx.Response.Header.Set("Content-type", "application/json")

		uri := string(ctx.Path())
		method := string(ctx.Method())
		switch {
		case uri == "/user" && method == http.MethodPost:
			userRegistrationHandler.Handle(ctx)
		case uri == "/purse/refill" && method == http.MethodPatch:
			purseRefillHandler.Handle(ctx)
		case uri == "/purse/transfer" && method == http.MethodPatch:
			purseTransferHandler.Handle(ctx)
		case uri == "/purse/history" && method == http.MethodPost:
			purseHistoryHandler.Handle(ctx)
		case uri == "/currency" && method == http.MethodPost:
			currencyUploadHandler.Handle(ctx)
		default:
			ctx.Error("not found", fasthttp.StatusNotFound)
		}
	}

	waitGroup := &sync.WaitGroup{}
	waitGroup.Add(1)

	server := &fasthttp.Server{
		Handler: handlers,
	}
	go func(wg *sync.WaitGroup) {
		defer wg.Done()
		err := server.ListenAndServe(":8080")
		if err != nil {
			log.Printf("error when listen and serve: %#v", err)
		}
	}(waitGroup)

	signalHandler := signal.NewSignalHandler(func() error {
		closeContext, closeCancel := context.WithTimeout(context.Background(), 5*time.Second)
		defer closeCancel()
		postgresDisconnectErr := postgresConnection.Close(closeContext)
		if postgresDisconnectErr != nil {
			log.Printf("error when postgres disconnect: %#v", postgresDisconnectErr)
		}
		shutdownErr := server.Shutdown()
		if shutdownErr != nil {
			log.Printf("error when server shutdown: %#v", shutdownErr)
		}

		currencyCache.Stop()

		log.Print("server is down")

		return nil
	})

	go signalHandler.Poll()

	waitGroup.Wait()
}
