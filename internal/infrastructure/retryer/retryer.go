package retryer

import "time"

func WithRetry(f func() error, attempts int, errors ...error) error {
	errMap := make(map[error]interface{}, len(errors))
	for _, err := range errors {
		errMap[err] = nil
	}
	for i := 0; i < attempts; i++ {
		err := f()
		_, isErrorRetryable := errMap[err]
		if !isErrorRetryable {
			return err
		}
		time.Sleep(3 * time.Second)
	}
	return nil
}
