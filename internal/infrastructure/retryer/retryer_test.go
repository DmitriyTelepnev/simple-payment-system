package retryer

import (
	"errors"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestWithRetry_RetryableError(t *testing.T) {
	expectedAttempts := 3
	actualAttempts := 0

	var testRetryableErr = errors.New("test")

	_ = WithRetry(
		func() error {
			actualAttempts++
			return testRetryableErr
		},
		expectedAttempts,
		testRetryableErr,
	)

	assert.Equal(t, expectedAttempts, actualAttempts)
}

func TestWithRetry_NonRetryableError(t *testing.T) {
	expectedAttempts := 3
	actualAttempts := 0

	var testRetryableErr = errors.New("test")
	var nonRetryableError = errors.New("non-retryable-error")

	returnedError := WithRetry(
		func() error {
			actualAttempts++
			return nonRetryableError
		},
		expectedAttempts,
		testRetryableErr,
	)

	assert.Errorf(t, returnedError, nonRetryableError.Error())
	assert.Equal(t, 1, actualAttempts)
}
