package currency

import "errors"

const (
	RUR = "RUR"
	EUR = "EUR"
	USD = "USD"
)

var availableCurrencies = map[string]interface{}{
	RUR: nil,
	EUR: nil,
	USD: nil,
}

var CurrencyNotSupported error = errors.New("currency not supported")

func IsValidCurrency(currency string) bool {
	_, ok := availableCurrencies[currency]
	return ok
}
