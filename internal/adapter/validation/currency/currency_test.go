package currency

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestIsValidCurrency(t *testing.T) {
	assert.True(t, IsValidCurrency(USD))
	assert.False(t, IsValidCurrency("YEN"))
}
