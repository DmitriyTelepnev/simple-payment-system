package dto

import (
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/adapter/http/dto/request/user"
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/adapter/validation/currency"
)

const (
	minUserNameLen = 5
	maxUserNameLen = 50
)

func IsLeadValid(lead user.Lead) bool {
	return isValidName(lead.Name) &&
		currency.IsValidCurrency(lead.Currency)
}

func isValidName(name string) bool {
	return len(name) >= minUserNameLen &&
		len(name) <= maxUserNameLen
}
