package dto

import (
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/adapter/http/dto/request/user"
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/adapter/validation/currency"
	"github.com/stretchr/testify/assert"
	"strings"
	"testing"
)

func TestIsLeadValid_InvalidNameLength_lessThanMin(t *testing.T) {
	lead := user.Lead{
		Name:     "a",
		Country:  "Russia",
		City:     "Novosibirsk",
		Currency: currency.USD,
	}

	assert.False(t, IsLeadValid(lead))
}

func TestIsLeadValid_InvalidNameLength_moreThanMax(t *testing.T) {
	lead := user.Lead{
		Name:     strings.Repeat("a", maxUserNameLen*2),
		Country:  "Russia",
		City:     "Novosibirsk",
		Currency: currency.USD,
	}

	assert.False(t, IsLeadValid(lead))
}

func TestIsLeadValid_InvalidCurrencyCode(t *testing.T) {
	lead := user.Lead{
		Name:     "testUser",
		Country:  "Russia",
		City:     "Novosibirsk",
		Currency: "TEST",
	}

	assert.False(t, IsLeadValid(lead))
}

func TestIsLeadValid_IsValid(t *testing.T) {
	lead := user.Lead{
		Name:     "testUser",
		Country:  "Russia",
		City:     "Novosibirsk",
		Currency: currency.USD,
	}

	assert.True(t, IsLeadValid(lead))
}
