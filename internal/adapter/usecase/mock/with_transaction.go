package mock

func WithTransaction(f func() error) error {
	return f()
}
