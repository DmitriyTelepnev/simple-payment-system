package usecase

type WithTransactionFunc func(func() error) error
