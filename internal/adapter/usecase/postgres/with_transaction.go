package postgres

import (
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/adapter/usecase"
	"context"
	"fmt"
	"github.com/jackc/pgx/v4"
	"time"
)

func WithTransaction(connection *pgx.Conn) usecase.WithTransactionFunc {
	return func(f func() error) error {
		beginContext, beginCancel := context.WithTimeout(context.Background(), 5*time.Second)
		defer beginCancel()
		transaction, beginTransactionError := connection.Begin(beginContext)
		if beginTransactionError != nil {
			if transaction != nil {
				rollbackContext, rollbackCancel := context.WithTimeout(context.Background(), 5*time.Second)
				defer rollbackCancel()
				_ = transaction.Rollback(rollbackContext)
			}

			return fmt.Errorf("can't start transaction")
		}

		funcError := f()

		var finishError error

		if funcError != nil {
			rollbackContext, rollbackCancel := context.WithTimeout(context.Background(), 5*time.Second)
			defer rollbackCancel()
			finishError = transaction.Rollback(rollbackContext)
		}

		commitContext, commitCancel := context.WithTimeout(context.Background(), 5*time.Second)
		defer commitCancel()
		finishError = transaction.Commit(commitContext)
		if finishError != nil {
			rollbackContext, rollbackCancel := context.WithTimeout(context.Background(), 5*time.Second)
			defer rollbackCancel()
			_ = transaction.Rollback(rollbackContext)
		}

		return funcError
	}
}
