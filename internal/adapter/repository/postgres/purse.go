package postgres

import (
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/domain/entity"
	"context"
	"fmt"
	"github.com/jackc/pgx/v4"
	"github.com/pkg/errors"
	"time"
)

type purseRepo struct {
	connection *pgx.Conn
}

func NewPurseRepo(connection *pgx.Conn) *purseRepo {
	return &purseRepo{connection: connection}
}

const purseSave = `
INSERT INTO purses (account, user_id, currency_code, balance)
VALUES ($1, $2, $3, $4)
ON CONFLICT (account) DO UPDATE SET balance = EXCLUDED.balance
`

func (repo *purseRepo) Save(purse entity.Purse) error {
	commandContext, commandCancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer commandCancel()
	command, commandErr := repo.connection.Exec(commandContext, purseSave, purse.Account, purse.UserUID, purse.Currency, purse.Balance)

	if commandErr != nil {
		return errors.Wrap(commandErr, "purse not saved")
	}

	if command.RowsAffected() < 1 {
		return fmt.Errorf("purse %s not saved", purse.Account)
	}
	return nil
}

const purseGetByAccount = "SELECT * FROM purses WHERE account = $1 FOR UPDATE"

func (repo *purseRepo) Get(account string) (*entity.Purse, error) {
	queryContext, queryCancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer queryCancel()
	row := repo.connection.QueryRow(queryContext, purseGetByAccount, account)
	purse := entity.Purse{}
	scanError := row.Scan(&purse.UID, &purse.Account, &purse.UserUID, &purse.Currency, &purse.Balance)
	if scanError == pgx.ErrNoRows {
		return nil, nil
	} else if scanError != nil {
		return nil, scanError
	}
	return &purse, nil
}
