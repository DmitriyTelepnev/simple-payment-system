package postgres

import (
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/domain/entity"
	"context"
	"github.com/jackc/pgx/v4"
	"github.com/pkg/errors"
	"log"
	"time"
)

type purseHistoryRepo struct {
	connection *pgx.Conn
}

func NewPurseHistoryRepo(connection *pgx.Conn) *purseHistoryRepo {
	return &purseHistoryRepo{connection: connection}
}

const purseHistorySave = `
INSERT INTO purse_history (account_to, operation_ts, account_from, operation_type, amount, currency, currency_course)
VALUES ($1, $2, $3, $4, $5, $6, $7)
`

func (repo *purseHistoryRepo) Save(row entity.PurseHistoryRow) error {
	commandContext, commandCancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer commandCancel()
	command, commandErr := repo.connection.Exec(
		commandContext,
		purseHistorySave,
		row.AccountTo,
		row.OperationTime,
		row.AccountFrom,
		row.OperationType,
		row.Amount,
		row.Currency,
		row.CurrencyCourse,
	)

	if commandErr != nil {
		return errors.Wrap(commandErr, "history not saved")
	}

	if command.RowsAffected() < 1 {
		return errors.New("history not saved")
	}

	return nil
}

const purseHistoryGetByDateAndAccount = `
SELECT * FROM purse_history
WHERE account_to = $1
	AND operation_ts >= to_timestamp(greatest($2, 0))
	AND operation_ts <= (SELECT CASE WHEN $3 = 0 THEN CURRENT_TIMESTAMP ELSE to_timestamp($3) END)
ORDER BY operation_ts
`

func (repo *purseHistoryRepo) GetByDateAndAccount(account string, timestampFrom int64, timestampTo int64) ([]entity.PurseHistoryRow, error) {
	queryContext, queryCommand := context.WithTimeout(context.Background(), 2*time.Second)
	defer queryCommand()

	rows, queryErr := repo.connection.Query(
		queryContext,
		purseHistoryGetByDateAndAccount,
		account,
		timestampFrom,
		timestampTo,
	)

	if queryErr != nil {
		return nil, errors.Wrap(queryErr, "can't fetch purse history")
	}

	queryHistory := []entity.PurseHistoryRow{}
	for rows.Next() {
		row := entity.PurseHistoryRow{}
		scanErr := rows.Scan(
			&row.AccountTo,
			&row.OperationTime,
			&row.AccountFrom,
			&row.OperationType,
			&row.Amount,
			&row.Currency,
			&row.CurrencyCourse,
		)
		if scanErr == nil {
			queryHistory = append(queryHistory, row)
		} else {
			log.Printf("[purse history repo] can't scan row: %#v", scanErr)
			return nil, errors.New("can't scan row")
		}
	}

	return queryHistory, nil
}
