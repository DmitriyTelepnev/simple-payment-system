package postgres

import (
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/domain/entity"
	"context"
	"fmt"
	"github.com/jackc/pgx/v4"
	"time"
)

type userRepo struct {
	connection *pgx.Conn
}

func NewUserRepo(connection *pgx.Conn) *userRepo {
	return &userRepo{
		connection: connection,
	}
}

const saveUser = `
INSERT INTO users (name, city, country)
VALUES ($1, $2, $3)
`

func (r *userRepo) Save(user entity.User) error {
	commandContext, commandCancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer commandCancel()
	command, commandErr := r.connection.Exec(commandContext, saveUser, user.Name, user.City, user.Country)

	if commandErr != nil {
		return commandErr
	}

	if command.RowsAffected() < 1 {
		return fmt.Errorf("user %s not saved", user.Name)
	}
	return nil
}

const getUser = "SELECT * FROM users WHERE name = $1 FOR UPDATE"

func (r *userRepo) Get(name string) (*entity.User, error) {
	queryContext, queryCancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer queryCancel()
	row := r.connection.QueryRow(queryContext, getUser, name)
	user := entity.User{}
	scanError := row.Scan(&user.UID, &user.Name, &user.Country, &user.City)

	if scanError == pgx.ErrNoRows {
		return nil, nil
	} else if scanError != nil {
		return nil, scanError
	}
	return &user, nil
}
