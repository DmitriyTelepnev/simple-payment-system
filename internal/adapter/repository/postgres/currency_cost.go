package postgres

import (
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/domain/entity"
	"context"
	"fmt"
	"github.com/jackc/pgx/v4"
	"github.com/pkg/errors"
	"time"
)

type currencyCostRepo struct {
	connection *pgx.Conn
}

func NewCurrencyCostRepo(connection *pgx.Conn) *currencyCostRepo {
	return &currencyCostRepo{connection: connection}
}

const currencyCostGetByCodeAndDate = "SELECT * FROM currency_costs WHERE currency_code = $1 AND date = $2"

func (repo *currencyCostRepo) Get(currencyCode string, date time.Time) (*entity.CurrencyCost, error) {
	queryContext, queryCancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer queryCancel()
	row := repo.connection.QueryRow(queryContext, currencyCostGetByCodeAndDate, currencyCode, date)
	currencyCost := entity.CurrencyCost{}
	scanError := row.Scan(&currencyCost.Currency, &currencyCost.Date, &currencyCost.Cost)
	if scanError == pgx.ErrNoRows {
		return nil, nil
	} else if scanError != nil {
		return nil, scanError
	}
	return &currencyCost, nil
}

const currencyCostSave = `
INSERT INTO currency_costs (currency_code, date, amount)
VALUES ($1, $2, $3)
`

func (repo *currencyCostRepo) Save(currencyCost entity.CurrencyCost) error {
	commandContext, commandCancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer commandCancel()
	command, commandErr := repo.connection.Exec(commandContext, currencyCostSave, currencyCost.Currency, currencyCost.Date, currencyCost.Cost)

	if commandErr != nil {
		return errors.Wrap(commandErr, "currencyCost not saved")
	}

	if command.RowsAffected() < 1 {
		return fmt.Errorf("currencyCost %s, %s not saved", currencyCost.Currency, currencyCost.Date)
	}
	return nil
}
