package mock

import (
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/domain/entity"
)

type purseHistory struct {
	errorOnGet  error
	errorOnSave error
	history     []entity.PurseHistoryRow
}

// Get Purse history repo mock
func NewPurseHistoryRepo(errorOnGet error, errorOnSave error) *purseHistory {
	return &purseHistory{
		errorOnGet:  errorOnGet,
		errorOnSave: errorOnSave,
		history:     []entity.PurseHistoryRow{},
	}
}

func (r *purseHistory) Save(row entity.PurseHistoryRow) error {
	if r.errorOnSave != nil {
		return r.errorOnSave
	}

	r.history = append(r.history, row)

	return nil
}

func (r *purseHistory) GetByDateAndAccount(account string, timestampFrom int64, timestampTo int64) ([]entity.PurseHistoryRow, error) {
	if r.errorOnGet != nil {
		return nil, r.errorOnGet
	}
	result := []entity.PurseHistoryRow{}
	for _, row := range r.history {
		if row.AccountTo == account && row.OperationTime.Unix() <= timestampTo && row.OperationTime.Unix() >= timestampFrom {
			result = append(result, row)
		}
	}

	return result, nil
}
