package mock

import (
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/domain/entity"
)

type user struct {
	errorOnGet  error
	errorOnSave error
	users       map[string]entity.User
}

// Get user repository mock
func NewUserRepo(errorOnGet error, errorOnSave error) *user {
	return &user{
		errorOnGet:  errorOnGet,
		errorOnSave: errorOnSave,
		users:       make(map[string]entity.User),
	}
}

func (r *user) Save(user entity.User) error {
	if r.errorOnSave != nil {
		return r.errorOnSave
	}

	r.users[user.Name] = user
	return nil
}

func (r *user) Get(name string) (*entity.User, error) {
	if r.errorOnGet != nil {
		return nil, r.errorOnGet
	}

	user, ok := r.users[name]
	if !ok {
		return nil, nil
	}
	return &user, nil
}
