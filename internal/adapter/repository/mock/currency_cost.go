package mock

import (
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/domain/entity"
	"fmt"
	"time"
)

type currencyCost struct {
	errorOnGet  error
	errorOnSave error
	costs       map[string]entity.CurrencyCost
}

// Get currency repo mock
func NewCurrencyCost(errorOnGet error, errorOnSave error) *currencyCost {
	return &currencyCost{
		errorOnGet:  errorOnGet,
		errorOnSave: errorOnSave,
		costs:       make(map[string]entity.CurrencyCost),
	}
}

func (r *currencyCost) Get(currencyCode string, date time.Time) (*entity.CurrencyCost, error) {
	if r.errorOnGet != nil {
		return nil, r.errorOnGet
	}

	cost, ok := r.costs[key(currencyCode, date)]
	if !ok {
		return nil, fmt.Errorf("currency cost %s on date %s not found", currencyCode, date)
	}

	return &cost, nil
}

func (r *currencyCost) Save(cost entity.CurrencyCost) error {
	if r.errorOnSave != nil {
		return r.errorOnSave
	}

	r.costs[key(cost.Currency, cost.Date)] = cost

	return nil
}

func key(currencyCode string, date time.Time) string {
	return fmt.Sprintf("%s-%s", currencyCode, date)
}
