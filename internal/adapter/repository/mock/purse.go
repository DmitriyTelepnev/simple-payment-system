package mock

import (
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/domain/entity"
	"fmt"
)

type purse struct {
	errorOnGet  error
	errorOnSave error
	purses      map[string]entity.Purse
}

// Get purse repo mock
func NewPurseRepo(errorOnGet error, errorOnSave error) *purse {
	return &purse{
		errorOnGet:  errorOnGet,
		errorOnSave: errorOnSave,
		purses:      make(map[string]entity.Purse),
	}
}

func (r *purse) Save(purse entity.Purse) error {
	r.purses[purse.Account] = purse
	if r.errorOnSave != nil {
		return r.errorOnSave
	}
	return nil
}

func (r *purse) Get(account string) (*entity.Purse, error) {
	if r.errorOnGet != nil {
		return nil, r.errorOnGet
	}
	purse, ok := r.purses[account]
	if !ok {
		return nil, fmt.Errorf("purse not found")
	}
	return &purse, nil
}
