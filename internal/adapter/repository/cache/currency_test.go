package cache

import (
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/adapter/repository/mock"
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/adapter/validation/currency"
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/domain/entity"
	cacheLib "bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/infrastructure/cache"
	"errors"
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func TestCachedCurrency_Save(t *testing.T) {
	currencyRepo := mock.NewCurrencyCost(nil, nil)
	cache := cacheLib.NewInmemoryWithTicker(3, 1*time.Second)
	cachedRepo := NewCachedCurrencyRepo(currencyRepo, cache)

	date, _ := time.Parse("2006-01-02", "2020-01-01")
	curr1 := entity.CurrencyCost{
		Currency: currency.USD,
		Cost:     1,
		Date:     date,
	}

	err := cachedRepo.Save(curr1)
	assert.NoError(t, err)

	cachedCurr1, _ := cache.Get(fmt.Sprintf("%s-%s", currency.USD, date.String()))
	assert.NotNil(t, cachedCurr1)
	castedCachedCurr1, ok := cachedCurr1.(entity.CurrencyCost)
	assert.True(t, ok)
	assert.Equal(t, curr1, castedCachedCurr1)

	cache.Stop()
}

func TestCachedCurrency_Save_ErrorOnSave(t *testing.T) {
	currencyRepo := mock.NewCurrencyCost(nil, errors.New("test"))
	cache := cacheLib.NewInmemoryWithTicker(3, 1*time.Second)
	cachedRepo := NewCachedCurrencyRepo(currencyRepo, cache)

	date, _ := time.Parse("2006-01-02", "2020-01-01")
	curr1 := entity.CurrencyCost{
		Currency: currency.USD,
		Cost:     1,
		Date:     date,
	}

	err := cachedRepo.Save(curr1)
	assert.Errorf(t, err, "test")

	cachedCurr1, _ := cache.Get(fmt.Sprintf("%s-%s", currency.USD, date.String()))
	assert.Nil(t, cachedCurr1)

	cache.Stop()
}

func TestCachedCurrency_Get(t *testing.T) {
	currencyRepo := mock.NewCurrencyCost(nil, nil)
	cache := cacheLib.NewInmemoryWithTicker(3, 1*time.Second)
	cachedRepo := NewCachedCurrencyRepo(currencyRepo, cache)

	date, _ := time.Parse("2006-01-02", "2020-01-01")
	curr1 := entity.CurrencyCost{
		Currency: currency.USD,
		Cost:     1,
		Date:     date,
	}

	_ = currencyRepo.Save(curr1)

	cachedCurr, _ := cachedRepo.Get(curr1.Currency, date)
	assert.Equal(t, &curr1, cachedCurr)

	cachedCurr1, _ := cache.Get(fmt.Sprintf("%s-%s", currency.USD, date.String()))
	assert.NotNil(t, cachedCurr1)
	castedCachedCurr1, ok := cachedCurr1.(entity.CurrencyCost)
	assert.True(t, ok)
	assert.Equal(t, curr1, castedCachedCurr1)

	cache.Stop()
}

func TestCachedCurrency_Get_ErrorOnGet(t *testing.T) {
	currencyRepo := mock.NewCurrencyCost(errors.New("test"), nil)
	cache := cacheLib.NewInmemoryWithTicker(3, 1*time.Second)
	cachedRepo := NewCachedCurrencyRepo(currencyRepo, cache)

	date, _ := time.Parse("2006-01-02", "2020-01-01")
	curr1 := entity.CurrencyCost{
		Currency: currency.USD,
		Cost:     1,
		Date:     date,
	}

	_ = currencyRepo.Save(curr1)

	cachedCurr, err := cachedRepo.Get(curr1.Currency, date)
	assert.Errorf(t, err, "test")
	assert.Nil(t, cachedCurr)

	cache.Stop()
}
