package cache

import (
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/domain/entity"
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/domain/repository"
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/infrastructure/cache"
	"log"
	"strings"
	"time"
)

type cachedCurrency struct {
	currencyRepo repository.CurrencyCost
	cache        cache.Cache
}

func NewCachedCurrencyRepo(currencyRepo repository.CurrencyCost, cache cache.Cache) *cachedCurrency {
	return &cachedCurrency{currencyRepo: currencyRepo, cache: cache}
}

func (repo *cachedCurrency) Get(currencyCode string, date time.Time) (*entity.CurrencyCost, error) {
	obj, err := repo.cache.Get(cacheKey(currencyCode, date))
	if err == nil && obj != nil {
		objFromCache := obj.(entity.CurrencyCost)
		return &objFromCache, nil
	}

	currencyCost, err := repo.currencyRepo.Get(currencyCode, date)
	if err != nil {
		return nil, err
	}

	err = repo.cache.Set(cacheKey(currencyCode, date), *currencyCost)
	if err != nil {
		log.Printf("[cached currency repo] can't set currency into cache: %#v\n", err)
	}

	return currencyCost, nil
}

func (repo *cachedCurrency) Save(currencyCost entity.CurrencyCost) error {
	saveErr := repo.currencyRepo.Save(currencyCost)
	if saveErr == nil {
		setCacheErr := repo.cache.Set(cacheKey(currencyCost.Currency, currencyCost.Date), currencyCost)
		if setCacheErr != nil {
			log.Printf("[cached currency repo] can't set currency into cache: %#v\n", setCacheErr)
		}
	}
	return saveErr
}

func cacheKey(currencyCode string, date time.Time) string {
	builder := strings.Builder{}
	builder.WriteString(currencyCode + "-" + date.String())
	return builder.String()
}
