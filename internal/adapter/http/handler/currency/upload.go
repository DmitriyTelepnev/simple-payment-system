package currency

import (
	currencyDto "bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/adapter/http/dto/request/currency"
	responseDto "bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/adapter/http/dto/response/currency"
	validationErr "bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/adapter/validation/currency"
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/usecase/currency"
	"encoding/json"
	"github.com/valyala/fasthttp"
	"log"
	"net/http"
	"time"
)

type upload struct {
	usecase *currency.Upload
}

func NewUpload(usecase *currency.Upload) *upload {
	return &upload{usecase: usecase}
}

// Currency
// @Summary UploadResponse a currency quotation
// @ID upload-currency
// @Accept  json
// @Produce  json
// @Param quotationObj body currency.CostUpload true "Currency quotation object"
// @Success 200 {object} currency.UploadResponse
// @Failure 400 {object} currency.UploadResponse
// @Failure 500 {object} currency.UploadResponse
// @Router /currency [post]
func (handler *upload) Handle(ctx *fasthttp.RequestCtx) {
	currencyCostUploadDto := currencyDto.CostUpload{}
	unmarshalErr := json.Unmarshal(ctx.Request.Body(), &currencyCostUploadDto)
	if unmarshalErr != nil {
		ctx.Response.SetStatusCode(http.StatusBadRequest)
		log.Printf("invalid request body, can't unmarshal. Raw body: %s", string(ctx.Request.Body()))
		return
	}

	date, dateParseError := time.Parse("2006-01-02", currencyCostUploadDto.Date)
	if dateParseError != nil {
		ctx.Response.SetStatusCode(http.StatusBadRequest)
		log.Printf("invalid currency cost date, can't parse. Raw body: %s", string(ctx.Request.Body()))
		return
	}

	usecaseError := handler.usecase.Execute(currencyCostUploadDto.Currency, currencyCostUploadDto.Cost, date)
	response := responseDto.UploadResponse{}

	if usecaseError != nil {
		log.Printf("%s, raw body: %s", usecaseError, string(ctx.Request.Body()))
		response.Error = usecaseError.Error()
	}

	ctx.Response.SetStatusCode(handler.determineHTTPStatusCode(usecaseError))

	body, marshalErr := json.Marshal(response)

	if marshalErr == nil {
		_, _ = ctx.Response.BodyWriter().Write(body)
	} else {
		ctx.Response.SetStatusCode(http.StatusInternalServerError)
	}
}

func (handler *upload) determineHTTPStatusCode(usecaseError error) int {
	code := http.StatusInternalServerError
	switch usecaseError {
	case validationErr.CurrencyNotSupported:
	case currency.CostIsLessThanOrEqualZero:
		code = http.StatusBadRequest
	}
	if usecaseError == nil {
		code = http.StatusOK
	}
	return code
}
