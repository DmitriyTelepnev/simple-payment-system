package user

import (
	userDto "bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/adapter/http/dto/request/user"
	responseDto "bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/adapter/http/dto/response/user"
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/usecase/user"
	"encoding/json"
	"github.com/valyala/fasthttp"
	"log"
	"net/http"
)

type registration struct {
	usecase *user.Registration
}

func NewRegistration(usecase *user.Registration) *registration {
	return &registration{
		usecase: usecase,
	}
}

// User
// @Summary Register user + create purse
// @ID user-register
// @Accept  json
// @Produce  json
// @Param userRegisterDto body user.Lead true "Register user and create purse"
// @Success 200 {object} user.Registration
// @Failure 400 {object} user.Registration
// @Failure 500 {object} user.Registration
// @Router /user [post]
func (handler *registration) Handle(ctx *fasthttp.RequestCtx) {
	lead := userDto.Lead{}

	unmarshalErr := json.Unmarshal(ctx.Request.Body(), &lead)
	if unmarshalErr != nil {
		ctx.Response.SetStatusCode(http.StatusBadRequest)
		log.Printf("invalid request body, can't unmarshal. Raw body: %s", string(ctx.Request.Body()))
		return
	}

	account, usecaseError := handler.usecase.Execute(lead)
	response := responseDto.Registration{
		Account: account,
	}

	if usecaseError != nil {
		log.Printf("%s, raw body: %s", usecaseError, string(ctx.Request.Body()))
		response.Error = usecaseError.Error()
	}

	ctx.Response.SetStatusCode(handler.determineHTTPStatusCode(usecaseError))
	responseBody, err := json.Marshal(response)

	if err == nil {
		_, _ = ctx.Response.BodyWriter().Write(responseBody)
	} else {
		ctx.Response.SetStatusCode(http.StatusInternalServerError)
	}
}

func (handler *registration) determineHTTPStatusCode(usecaseError error) int {
	code := http.StatusInternalServerError
	switch usecaseError {
	case user.InvalidRequest:
	case user.UserAlreadyExists:
		code = http.StatusBadRequest
	case user.CantGenerateAccount:
	case user.CantSaveUser:
	case user.CantCreatePurse:
		code = http.StatusInternalServerError
	}
	if usecaseError == nil {
		code = http.StatusOK
	}
	return code
}
