package purse

import (
	purseDto "bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/adapter/http/dto/request/purse"
	purseHistory "bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/adapter/http/dto/response/purse"
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/domain/service/currency"
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/usecase/purse"
	"encoding/json"
	"github.com/valyala/fasthttp"
	"log"
	"net/http"
)

type transfer struct {
	usecase *purse.Transfer
}

func NewTransfer(usecase *purse.Transfer) *transfer {
	return &transfer{usecase: usecase}
}

// Purse
// @Summary TransferResponse money from account to account
// @ID purse-refill
// @Accept  json
// @Produce  json
// @Param purseTransferDto body purse.Transfer true "TransferResponse money from account to account"
// @Success 200 {object} purse.TransferResponse
// @Failure 400 {object} purse.TransferResponse
// @Failure 500 {object} purse.TransferResponse
// @Router /purse/transfer [patch]
func (handler *transfer) Handle(ctx *fasthttp.RequestCtx) {
	transferDto := purseDto.Transfer{}
	unmarshalErr := json.Unmarshal(ctx.Request.Body(), &transferDto)
	if unmarshalErr != nil {
		ctx.Response.SetStatusCode(http.StatusBadRequest)
		log.Printf("invalid request body, can't unmarshal. Raw body: %s", string(ctx.Request.Body()))
		return
	}

	usecaseError := handler.usecase.Execute(transferDto)
	response := purseHistory.TransferResponse{}
	if usecaseError != nil {
		log.Printf("%s, raw body: %s", usecaseError, string(ctx.Request.Body()))
		response.Error = usecaseError.Error()
	}

	ctx.Response.SetStatusCode(handler.determineHTTPStatusCode(usecaseError))

	body, marshalErr := json.Marshal(response)

	if marshalErr == nil {
		_, _ = ctx.Response.BodyWriter().Write(body)
	} else {
		ctx.Response.SetStatusCode(http.StatusInternalServerError)
	}
}

func (handler *transfer) determineHTTPStatusCode(usecaseError error) int {
	code := http.StatusInternalServerError
	switch usecaseError {
	case currency.ErrCantConvertCurrencies:
	case purse.InsufficientFunds:
	case purse.CantTransferMoney:
		code = http.StatusForbidden
	case purse.PurseNotFound:
		code = http.StatusNotFound
	}
	if usecaseError == nil {
		code = http.StatusOK
	}
	return code
}
