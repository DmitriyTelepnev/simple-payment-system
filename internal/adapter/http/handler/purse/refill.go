package purse

import (
	purseDto "bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/adapter/http/dto/request/purse"
	purseHistory "bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/adapter/http/dto/response/purse"
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/domain/service/currency"
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/usecase/purse"
	"encoding/json"
	"github.com/valyala/fasthttp"
	"log"
	"net/http"
)

type refill struct {
	usecase *purse.Refill
}

func NewRefill(usecase *purse.Refill) *refill {
	return &refill{usecase: usecase}
}

// Purse
// @Summary Purse refill
// @ID purse-refill
// @Accept  json
// @Produce  json
// @Param purseRefillDto body purse.Refill true "Purse refill"
// @Success 200 {object} purse.RefillResponse
// @Failure 400 {object} purse.RefillResponse
// @Failure 500 {object} purse.RefillResponse
// @Router /purse/refill [patch]
func (handler *refill) Handle(ctx *fasthttp.RequestCtx) {
	refillDto := purseDto.Refill{}
	unmarshalErr := json.Unmarshal(ctx.Request.Body(), &refillDto)
	if unmarshalErr != nil {
		ctx.Response.SetStatusCode(http.StatusBadRequest)
		log.Printf("invalid request body, can't unmarshal. Raw body: %s", string(ctx.Request.Body()))
		return
	}

	usecaseError := handler.usecase.Execute(refillDto)
	response := purseHistory.RefillResponse{}
	if usecaseError != nil {
		log.Printf("%s, raw body: %s", usecaseError, string(ctx.Request.Body()))
		response.Error = usecaseError.Error()
	}
	ctx.Response.SetStatusCode(handler.determineHTTPStatusCode(usecaseError))

	body, marshalErr := json.Marshal(response)

	if marshalErr == nil {
		_, _ = ctx.Response.BodyWriter().Write(body)
	} else {
		ctx.Response.SetStatusCode(http.StatusInternalServerError)
	}
}

func (handler *refill) determineHTTPStatusCode(usecaseError error) int {
	code := http.StatusInternalServerError
	switch usecaseError {
	case currency.ErrCantConvertCurrencies:
	case purse.CantRefillPurse:
		code = http.StatusForbidden
	case purse.PurseNotFound:
		code = http.StatusNotFound
	}
	if usecaseError == nil {
		code = http.StatusOK
	}
	return code
}
