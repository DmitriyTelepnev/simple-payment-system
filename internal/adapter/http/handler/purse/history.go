package purse

import (
	purseDto "bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/adapter/http/dto/request/purse"
	purseHistory "bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/adapter/http/dto/response/purse"
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/infrastructure/config"
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/usecase/purse"
	"encoding/json"
	"github.com/valyala/fasthttp"
	"log"
	"net/http"
)

type history struct {
	usecase *purse.History
}

func NewHistory(usecase *purse.History) *history {
	return &history{usecase: usecase}
}

// Purse
// @Summary Get purse history
// @ID purse-history
// @Accept  json
// @Produce  json
// @Param purseHistoryDto body purse.GetHistory true "Get purse history by account"
// @Success 200 {object} purse.History
// @Failure 400 {object} purse.History
// @Failure 500 {object} purse.History
// @Router /purse/history [post]
func (handler *history) Handle(ctx *fasthttp.RequestCtx) {
	historyDto := purseDto.GetHistory{}
	unmarshalErr := json.Unmarshal(ctx.Request.Body(), &historyDto)
	if unmarshalErr != nil {
		ctx.Response.SetStatusCode(http.StatusBadRequest)
		log.Printf("invalid request body, can't unmarshal. Raw body: %s", string(ctx.Request.Body()))
		return
	}

	rows, usecaseError := handler.usecase.Execute(historyDto.Account, historyDto.TimestampFrom, historyDto.TimestampTo)
	response := purseHistory.History{}
	if usecaseError != nil {
		log.Printf("%s, raw body: %s", usecaseError, string(ctx.Request.Body()))
		response.Error = usecaseError.Error()
	}

	ctx.Response.SetStatusCode(handler.determineHTTPStatusCode(usecaseError))

	historyRows := make([]purseHistory.HistoryRow, len(rows))
	for i := range rows {
		historyRows[i] = purseHistory.HistoryRow{
			AccountTo:      rows[i].AccountTo,
			OperationTime:  rows[i].OperationTime.Unix(),
			AccountFrom:    rows[i].AccountFrom,
			OperationType:  rows[i].OperationType,
			Amount:         float64(rows[i].Amount) / config.CurrencyBaseDenominator,
			Currency:       rows[i].Currency,
			CurrencyCourse: float64(rows[i].CurrencyCourse) / config.CurrencyBaseDenominator,
		}
	}

	response.Rows = historyRows
	body, marshalErr := json.Marshal(response)

	if marshalErr == nil {
		_, _ = ctx.Response.BodyWriter().Write(body)
	} else {
		ctx.Response.SetStatusCode(http.StatusInternalServerError)
	}
}

func (handler *history) determineHTTPStatusCode(usecaseError error) int {
	code := http.StatusInternalServerError
	switch usecaseError {
	case purse.DateValidationError:
		code = http.StatusBadRequest
	}
	if usecaseError == nil {
		code = http.StatusOK
	}
	return code
}
