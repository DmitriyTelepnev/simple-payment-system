package purse

// Transfer request
type Transfer struct {
	AccountFrom string  `json:"accountFrom"`
	AccountTo   string  `json:"accountTo"`
	Amount      float64 `json:"amount"`
	Currency    string  `json:"currency"`
}
