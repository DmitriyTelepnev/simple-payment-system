package purse

// Get purse history body request
type GetHistory struct {
	Account       string `json:"account"`
	TimestampFrom int64  `json:"timestampFrom,omitempty"`
	TimestampTo   int64  `json:"timestampTo,omitempty"`
}
