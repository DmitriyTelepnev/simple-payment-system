package purse

// Refill request
type Refill struct {
	Account  string  `json:"account"`
	Currency string  `json:"currency"`
	Amount   float64 `json:"amount"`
}
