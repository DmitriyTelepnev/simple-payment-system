package user

import (
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/domain/entity"
)

// User registration request
type Lead struct {
	Name     string `json:"name"`
	Country  string `json:"country"`
	City     string `json:"city"`
	Currency string `json:"currency"`
}

func LeadToUser(lead Lead) entity.User {
	return entity.User{
		Name:    lead.Name,
		Country: lead.Country,
		City:    lead.City,
	}
}
