package currency

// Currency Quotation upload dto
type CostUpload struct {
	Currency string  `json:"currency"`
	Cost     float64 `json:"cost"`
	Date     string  `json:"date"`
}
