package user

// User registration response
type Registration struct {
	Error   string `json:"error,omitempty"`
	Account string `json:"account,omitempty"`
}
