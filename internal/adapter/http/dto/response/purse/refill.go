package purse

// Purse refill response
type RefillResponse struct {
	Error string `json:"error,omitempty"`
}
