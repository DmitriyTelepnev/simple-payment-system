package purse

// Transfer money from account to account response
type TransferResponse struct {
	Error string `json:"error,omitempty"`
}
