package purse

// History item
type HistoryRow struct {
	AccountTo      string  `json:"accountTo"`
	OperationTime  int64   `json:"operationTimestamp"`
	AccountFrom    string  `json:"accountFrom"`
	OperationType  int     `json:"operationType"`
	Amount         float64 `json:"amount"`
	Currency       string  `json:"currency"`
	CurrencyCourse float64 `json:"currencyCourse"`
}

// Purse history response
type History struct {
	Error string       `json:"error,omitempty"`
	Rows  []HistoryRow `json:"history"`
}
