package currency

// Currency quotation upload response
type UploadResponse struct {
	Error string `json:"error,omitempty"`
}
