package repository

import (
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/domain/entity"
)

// User repository
type User interface {
	Save(user entity.User) error
	Get(name string) (*entity.User, error)
}
