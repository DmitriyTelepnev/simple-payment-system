package repository

import (
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/domain/entity"
	"errors"
	"time"
)

var ErrCantSaveCurrency error = errors.New("can't save currency cost")
var ErrCurrencyNotFound error = errors.New("currency not found")

// Currency quotation repository
type CurrencyCost interface {
	Get(currencyCode string, date time.Time) (*entity.CurrencyCost, error)
	Save(currencyCost entity.CurrencyCost) error
}
