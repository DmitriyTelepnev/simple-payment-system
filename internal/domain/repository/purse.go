package repository

import (
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/domain/entity"
)

// Purse repository
type Purse interface {
	Save(purse entity.Purse) error
	Get(account string) (*entity.Purse, error)
}
