package repository

import (
	"bitbucket.org/DmitriyTelepnev/simple-payment-system/internal/domain/entity"
)

// Purse history repository
type PurseHistory interface {
	Save(row entity.PurseHistoryRow) error
	GetByDateAndAccount(account string, timestampFrom int64, timestampTo int64) ([]entity.PurseHistoryRow, error)
}
