package currency

import "errors"

var ErrCantConvertCurrencies error = errors.New("can't convert currencies")

type Converter interface {
	Convert(amount int64, currencyFrom string, currencyTo string) (int64, error)
}
