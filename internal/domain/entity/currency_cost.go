package entity

import "time"

type CurrencyCost struct {
	Currency string
	Cost     float64
	Date     time.Time
}
