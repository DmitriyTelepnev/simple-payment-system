package entity

import "time"

const (
	OperationCreate = iota
	OperationRefill
	OperationTransfer
)

type PurseHistoryRow struct {
	AccountTo      string    `db:"account_to"`
	OperationTime  time.Time `db:"operation_ts"`
	AccountFrom    string    `db:"account_from"`
	OperationType  int       `db:"operation_type"`
	Amount         int64     `db:"amount"`
	Currency       string    `db:"currency"`
	CurrencyCourse int64     `db:"currency_course"`
}
