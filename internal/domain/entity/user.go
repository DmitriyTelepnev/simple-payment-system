package entity

type User struct {
	UID     int64  `db:"uid"`
	Name    string `db:"name"`
	Country string `db:"country"`
	City    string `db:"city"`
}
