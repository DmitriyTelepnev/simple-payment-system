package entity

type Purse struct {
	UID      int64  `db:"uid"`
	Account  string `db:"account"`
	UserUID  int64  `db:"user_id"`
	Currency string `db:"currency_code"`
	Balance  int64  `db:"balance"`
}
