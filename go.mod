module bitbucket.org/DmitriyTelepnev/simple-payment-system

go 1.13

require (
	github.com/alecthomas/template v0.0.0-20190718012654-fb15b899a751
	github.com/andybalholm/brotli v1.0.1 // indirect
	github.com/gofrs/uuid v3.3.0+incompatible
	github.com/jackc/pgx/v4 v4.9.0
	github.com/klauspost/compress v1.11.1 // indirect
	github.com/pkg/errors v0.8.1
	github.com/stretchr/testify v1.6.1
	github.com/swaggo/swag v1.6.8
	github.com/valyala/fasthttp v1.16.0
	gopkg.in/yaml.v3 v3.0.0-20200313102051-9f266ea9e77c
)
